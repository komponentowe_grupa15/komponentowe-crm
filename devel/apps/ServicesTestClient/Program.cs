﻿using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesTestClient
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("--------------Test Client For WCF services--------------");
            Console.WriteLine();
            ClientServiceWrapper.MainManager wrap = ClientServiceWrapper.MainManager.Instance;
            ServiceReference1.PracownicyClient cl = new ServiceReference1.PracownicyClient();
            var a = cl.ZwrocPracownikow();
            
            Console.WriteLine(wrap.Klienci.CheckConnection());
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("--------------Test Client For WCF services--------------");
            Console.ReadKey();

        }
    }
}
