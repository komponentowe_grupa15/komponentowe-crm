﻿using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using CustomerRelationshipManagement.DAL;
using CustomService;
using ImportEksportService;
using KlienciService;
using LogService;
using MailingService;
using OrganizerService;
using PK.ERP.PracownicyService;
using ShopManager.Common.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace ClientServiceWrapper
{
    public class MainManager
    {
        private List<Historia> historia;
        public Uzytkownicy UserInfo { get; set; }
        

        private static MainManager _instance;

        public static MainManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MainManager();

                }
                return _instance;
            }
        }
        public MainManager()
        {
            historia = new List<Historia>();
            RegisterComponents();
        }

        private CacheManager _cacheManager;

        public CacheManager CacheManager
        {
            get
            {
                if (_cacheManager == null)
                {
                    _cacheManager = new CacheManager();
                }
                return _cacheManager;
            }
        }


        public ICustomService CustomServiceInstance
        {
            get
            {
                return IoC.IocContainer.Instance.Container.Resolve<ICustomService>();
            }

        }

        public IImportEksport ImportEksportKontaktow
        {
            get
            {
                return IoC.IocContainer.Instance.Container.Resolve<IImportEksport>();
            }
        }


        public IKlienciService Klienci
        {
            get
            {
                return IoC.IocContainer.Instance.Container.Resolve<IKlienciService>();
            }
        }


        public IOrganizer Organizer
        {
            get
            {
                return IoC.IocContainer.Instance.Container.Resolve<IOrganizer>();
            }
        }


        public IShopManagerProducts Products
        {
            get
            {
                return IoC.IocContainer.Instance.Container.Resolve<IShopManagerProducts>();
            }
        }

        public ILogService Logger
        {
            get
            {
                return IoC.IocContainer.Instance.Container.Resolve<ILogService>();
            }
        }

        public IMailingService MailingService
        {
            get
            {
                return IoC.IocContainer.Instance.Container.Resolve<IMailingService>();
            }
        }

        public void AddLog(Historia log)
        {

            historia.Add(log);
        }

        public List<Historia> GetLogs()
        {
            return historia;
        }

        public IPracownicy Pracownicy
        {
            get 
            {
                return IoC.IocContainer.Instance.Resolve<IPracownicy>();
            }
        }

        public string CheckConncetion()
        {
            var custom = this.CustomServiceInstance.CheckConnection();
            var custom2 = this.Klienci.CheckConnection();
       
            return custom + custom2;
        }


        private static void RegisterComponents()
        {
            IoC.IocContainer.Instance.Container.Register(Component.For<ICustomerManagmentDataEntities>()
                .ImplementedBy<CustomerManagmentDataEntities2>().LifeStyle.Singleton);
            InstallClientForService<ICustomService>("http://wolanin.studentlive.pl/customservice.svc");
            InstallClientForService<IImportEksport>("http://wolanin.studentlive.pl/ImportEksportService.svc");
            InstallClientForService<IKlienciService>("http://wolanin.studentlive.pl/KlienciService.svc");
            InstallClientForService<IMailingService>("http://wolanin.studentlive.pl/MailingService.svc");
            InstallClientForService<IOrganizer>("http://wolanin.studentlive.pl/Organizer.svc");
            InstallClientForService<ILogService>("http://wolanin.studentlive.pl/LogService.svc");

            //komponenty innych grup
            InstallClientForService<IShopManagerProducts>("http://mstest.studentlive.pl/ShopManagerProducts.svc");
            InstallClientForService<IPracownicy>("http://mstest.studentlive.pl/AD/Pracownicy.svc");
        }

        private static void InstallClientForService<T>(string address) where T : class
        {
            var binding = new BasicHttpBinding();
            //binding.TextEncoding = Encoding.UTF8;
            //binding.MessageEncoding = WSMessageEncoding.Text;
            binding.MaxReceivedMessageSize = 65536;
            IoC.IocContainer.Instance.Container.Register(Component.For<T>().AsWcfClient<T>(new DefaultClientModel()
                .Endpoint = WcfEndpoint.FromEndpoint(new ServiceEndpoint(ContractDescription.GetContract(typeof(T)),
                   binding,
                    new EndpointAddress(address)))));

        }
    }


    public class CacheManager
    {
        private Dictionary<string, object> _cache;

        public CacheManager()
        {
            _cache = new Dictionary<string, object>();
        }

        public void Add<T>(string key, object a) where T : class
        {
            _cache[key] =  a;
        }

        public T GetValue<T>(string key) where T : class
        {
            object value;
            if (_cache.TryGetValue(key, out value))
            {
                return value as T;
            }
            return null;
        }
    }


}
