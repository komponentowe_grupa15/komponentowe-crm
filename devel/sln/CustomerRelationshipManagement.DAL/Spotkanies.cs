//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CustomerRelationshipManagement.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Spotkanies
    {
        public int SpotkanieID { get; set; }
        public string Nazwa { get; set; }
        public System.DateTime Data { get; set; }
        public double DlugoscTrwania { get; set; }
        public string Kolor { get; set; }
        public int UzytkownikId { get; set; }
    
        public virtual Uzytkownicy Uzytkownicy { get; set; }
    }
}
