
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 01/12/2013 09:52:22
-- Generated from EDMX file: D:\Studia\informatyka\ROK III\programowanie komponentowe\projekt\komponentowe-crm\devel\sln\CustomerRelationshipManagement.DAL\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [wolanin.studentlive.pl];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_BranzaKlientaKlienci]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Klienci] DROP CONSTRAINT [FK_BranzaKlientaKlienci];
GO
IF OBJECT_ID(N'[dbo].[FK_OsobaUzytkownicy]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Uzytkownicy] DROP CONSTRAINT [FK_OsobaUzytkownicy];
GO
IF OBJECT_ID(N'[dbo].[FK_PanstwaAdresy]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Adresy] DROP CONSTRAINT [FK_PanstwaAdresy];
GO
IF OBJECT_ID(N'[dbo].[FK_RodzajAdresuAdresy]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Adresy] DROP CONSTRAINT [FK_RodzajAdresuAdresy];
GO
IF OBJECT_ID(N'[dbo].[FK_StatusKlientaKlienci]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Klienci] DROP CONSTRAINT [FK_StatusKlientaKlienci];
GO
IF OBJECT_ID(N'[dbo].[FK_UzytkownicyKlienci]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Klienci] DROP CONSTRAINT [FK_UzytkownicyKlienci];
GO
IF OBJECT_ID(N'[dbo].[FK_UzytkownicyNotatki]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Notatki] DROP CONSTRAINT [FK_UzytkownicyNotatki];
GO
IF OBJECT_ID(N'[dbo].[FK_UzytkownicySpotkanies]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Spotkanies] DROP CONSTRAINT [FK_UzytkownicySpotkanies];
GO
IF OBJECT_ID(N'[dbo].[FK_WojewodztwaAdresy]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Adresy] DROP CONSTRAINT [FK_WojewodztwaAdresy];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Adresy]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Adresy];
GO
IF OBJECT_ID(N'[dbo].[AdresyKlientow]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AdresyKlientow];
GO
IF OBJECT_ID(N'[dbo].[BranzaKlienta]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BranzaKlienta];
GO
IF OBJECT_ID(N'[dbo].[Hasla]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Hasla];
GO
IF OBJECT_ID(N'[dbo].[Historia]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Historia];
GO
IF OBJECT_ID(N'[dbo].[Klienci]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Klienci];
GO
IF OBJECT_ID(N'[dbo].[Notatki]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Notatki];
GO
IF OBJECT_ID(N'[dbo].[NotatkiUzytkownikow]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NotatkiUzytkownikow];
GO
IF OBJECT_ID(N'[dbo].[Osoba]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Osoba];
GO
IF OBJECT_ID(N'[dbo].[Panstwa]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Panstwa];
GO
IF OBJECT_ID(N'[dbo].[RodzajAdresu]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RodzajAdresu];
GO
IF OBJECT_ID(N'[dbo].[Spotkanies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Spotkanies];
GO
IF OBJECT_ID(N'[dbo].[StatusKlienta]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StatusKlienta];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[Uzytkownicy]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Uzytkownicy];
GO
IF OBJECT_ID(N'[dbo].[Wiadomosci]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Wiadomosci];
GO
IF OBJECT_ID(N'[dbo].[Wojewodztwa]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Wojewodztwa];
GO
IF OBJECT_ID(N'[dbo].[Zadania]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Zadania];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Adresy'
CREATE TABLE [dbo].[Adresy] (
    [AdresId] int IDENTITY(1,1) NOT NULL,
    [Ulica_i_Numer] nvarchar(165)  NOT NULL,
    [Kod_Pocztowy] nvarchar(10)  NOT NULL,
    [Miejscowość] nvarchar(50)  NOT NULL,
    [WojewodztwoId] int  NOT NULL,
    [Telefon] nvarchar(60)  NOT NULL,
    [Email] nvarchar(60)  NOT NULL,
    [RodzajAdresuID] int  NOT NULL,
    [PanstwoId] int  NOT NULL
);
GO

-- Creating table 'Hasla'
CREATE TABLE [dbo].[Hasla] (
    [UzytkownikId] int  NOT NULL,
    [Salt] nvarchar(256)  NOT NULL,
    [Hash] nvarchar(300)  NOT NULL
);
GO

-- Creating table 'Historia'
CREATE TABLE [dbo].[Historia] (
    [HistoriaID] int IDENTITY(1,1) NOT NULL,
    [Akcja] nvarchar(50)  NOT NULL,
    [Data] datetime  NOT NULL,
    [UzytkownikID] int  NOT NULL
);
GO

-- Creating table 'Klienci'
CREATE TABLE [dbo].[Klienci] (
    [KlientId] int IDENTITY(1,1) NOT NULL,
    [Nazwa_Klienta] nvarchar(120)  NOT NULL,
    [Branza_Id] int  NOT NULL,
    [NIP] nvarchar(50)  NOT NULL,
    [REGON] nvarchar(50)  NOT NULL,
    [Status_Klienta_Id] int  NOT NULL,
    [Strona_WWW] nvarchar(100)  NOT NULL,
    [Uwagi] nvarchar(1000)  NOT NULL,
    [UzytkownikId] int  NOT NULL
);
GO

-- Creating table 'Notatki'
CREATE TABLE [dbo].[Notatki] (
    [NotatkaID] int IDENTITY(1,1) NOT NULL,
    [Notatka] nvarchar(2000)  NOT NULL,
    [Tytul] nvarchar(50)  NOT NULL,
    [Data_dodania] datetime  NOT NULL,
    [UzytkownikId] int  NOT NULL
);
GO

-- Creating table 'Osoba'
CREATE TABLE [dbo].[Osoba] (
    [OsobaId] int IDENTITY(1,1) NOT NULL,
    [Imie] nvarchar(50)  NOT NULL,
    [Nazwisko] nvarchar(50)  NOT NULL,
    [Data_Urodzenia] datetime  NOT NULL,
    [Data_Utworzenia] datetime  NOT NULL,
    [Aktywny] bit  NOT NULL
);
GO

-- Creating table 'Spotkanies'
CREATE TABLE [dbo].[Spotkanies] (
    [SpotkanieID] int IDENTITY(1,1) NOT NULL,
    [Nazwa] nvarchar(50)  NOT NULL,
    [Data] datetime  NOT NULL,
    [DlugoscTrwania] float  NOT NULL,
    [Kolor] nvarchar(50)  NULL,
    [UzytkownikId] int  NOT NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'Uzytkownicy'
CREATE TABLE [dbo].[Uzytkownicy] (
    [UzytkownikId] int IDENTITY(1,1) NOT NULL,
    [Login] nvarchar(18)  NOT NULL,
    [OsobaId] int  NOT NULL,
    [Aktywny] bit  NOT NULL
);
GO

-- Creating table 'Wojewodztwa'
CREATE TABLE [dbo].[Wojewodztwa] (
    [Id] int  NOT NULL,
    [Wojewodztwo] nvarchar(100)  NOT NULL
);
GO

-- Creating table 'NotatkiUzytkownikow'
CREATE TABLE [dbo].[NotatkiUzytkownikow] (
    [NotatkaId] int  NOT NULL,
    [UzytkownikId] int  NOT NULL
);
GO

-- Creating table 'RodzajAdresu'
CREATE TABLE [dbo].[RodzajAdresu] (
    [RodzajAdresuID] int IDENTITY(1,1) NOT NULL,
    [Rodzaj] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'Panstwa'
CREATE TABLE [dbo].[Panstwa] (
    [PanstwoID] int  NOT NULL,
    [Panstwo] nvarchar(70)  NOT NULL
);
GO

-- Creating table 'BranzaKlienta'
CREATE TABLE [dbo].[BranzaKlienta] (
    [BranzaID] int IDENTITY(1,1) NOT NULL,
    [Branza] nvarchar(100)  NOT NULL
);
GO

-- Creating table 'StatusKlienta'
CREATE TABLE [dbo].[StatusKlienta] (
    [StatusId] int IDENTITY(1,1) NOT NULL,
    [Status] nvarchar(70)  NOT NULL
);
GO

-- Creating table 'AdresyKlientow'
CREATE TABLE [dbo].[AdresyKlientow] (
    [AdresId] int  NOT NULL,
    [KlientId] int  NOT NULL,
    [AdresyKlientowID] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'Zadania'
CREATE TABLE [dbo].[Zadania] (
    [ZadanieId] int IDENTITY(1,1) NOT NULL,
    [Tekst] nvarchar(100)  NOT NULL,
    [Data] datetime  NOT NULL,
    [Priorytet] int  NULL,
    [UzytkownikId] int  NOT NULL
);
GO

-- Creating table 'Wiadomosci'
CREATE TABLE [dbo].[Wiadomosci] (
    [WadiomoscID] int  NOT NULL,
    [OdKogo] nvarchar(150)  NOT NULL,
    [DoKogo] nvarchar(150)  NOT NULL,
    [Temat] nvarchar(80)  NOT NULL,
    [Tresc] nvarchar(250)  NOT NULL,
    [Data] datetime  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AdresId] in table 'Adresy'
ALTER TABLE [dbo].[Adresy]
ADD CONSTRAINT [PK_Adresy]
    PRIMARY KEY CLUSTERED ([AdresId] ASC);
GO

-- Creating primary key on [UzytkownikId] in table 'Hasla'
ALTER TABLE [dbo].[Hasla]
ADD CONSTRAINT [PK_Hasla]
    PRIMARY KEY CLUSTERED ([UzytkownikId] ASC);
GO

-- Creating primary key on [HistoriaID] in table 'Historia'
ALTER TABLE [dbo].[Historia]
ADD CONSTRAINT [PK_Historia]
    PRIMARY KEY CLUSTERED ([HistoriaID] ASC);
GO

-- Creating primary key on [KlientId] in table 'Klienci'
ALTER TABLE [dbo].[Klienci]
ADD CONSTRAINT [PK_Klienci]
    PRIMARY KEY CLUSTERED ([KlientId] ASC);
GO

-- Creating primary key on [NotatkaID] in table 'Notatki'
ALTER TABLE [dbo].[Notatki]
ADD CONSTRAINT [PK_Notatki]
    PRIMARY KEY CLUSTERED ([NotatkaID] ASC);
GO

-- Creating primary key on [OsobaId] in table 'Osoba'
ALTER TABLE [dbo].[Osoba]
ADD CONSTRAINT [PK_Osoba]
    PRIMARY KEY CLUSTERED ([OsobaId] ASC);
GO

-- Creating primary key on [SpotkanieID] in table 'Spotkanies'
ALTER TABLE [dbo].[Spotkanies]
ADD CONSTRAINT [PK_Spotkanies]
    PRIMARY KEY CLUSTERED ([SpotkanieID] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [UzytkownikId] in table 'Uzytkownicy'
ALTER TABLE [dbo].[Uzytkownicy]
ADD CONSTRAINT [PK_Uzytkownicy]
    PRIMARY KEY CLUSTERED ([UzytkownikId] ASC);
GO

-- Creating primary key on [Id] in table 'Wojewodztwa'
ALTER TABLE [dbo].[Wojewodztwa]
ADD CONSTRAINT [PK_Wojewodztwa]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [NotatkaId] in table 'NotatkiUzytkownikow'
ALTER TABLE [dbo].[NotatkiUzytkownikow]
ADD CONSTRAINT [PK_NotatkiUzytkownikow]
    PRIMARY KEY CLUSTERED ([NotatkaId] ASC);
GO

-- Creating primary key on [RodzajAdresuID] in table 'RodzajAdresu'
ALTER TABLE [dbo].[RodzajAdresu]
ADD CONSTRAINT [PK_RodzajAdresu]
    PRIMARY KEY CLUSTERED ([RodzajAdresuID] ASC);
GO

-- Creating primary key on [PanstwoID] in table 'Panstwa'
ALTER TABLE [dbo].[Panstwa]
ADD CONSTRAINT [PK_Panstwa]
    PRIMARY KEY CLUSTERED ([PanstwoID] ASC);
GO

-- Creating primary key on [BranzaID] in table 'BranzaKlienta'
ALTER TABLE [dbo].[BranzaKlienta]
ADD CONSTRAINT [PK_BranzaKlienta]
    PRIMARY KEY CLUSTERED ([BranzaID] ASC);
GO

-- Creating primary key on [StatusId] in table 'StatusKlienta'
ALTER TABLE [dbo].[StatusKlienta]
ADD CONSTRAINT [PK_StatusKlienta]
    PRIMARY KEY CLUSTERED ([StatusId] ASC);
GO

-- Creating primary key on [AdresyKlientowID] in table 'AdresyKlientow'
ALTER TABLE [dbo].[AdresyKlientow]
ADD CONSTRAINT [PK_AdresyKlientow]
    PRIMARY KEY CLUSTERED ([AdresyKlientowID] ASC);
GO

-- Creating primary key on [ZadanieId] in table 'Zadania'
ALTER TABLE [dbo].[Zadania]
ADD CONSTRAINT [PK_Zadania]
    PRIMARY KEY CLUSTERED ([ZadanieId] ASC);
GO

-- Creating primary key on [WadiomoscID] in table 'Wiadomosci'
ALTER TABLE [dbo].[Wiadomosci]
ADD CONSTRAINT [PK_Wiadomosci]
    PRIMARY KEY CLUSTERED ([WadiomoscID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UzytkownikId] in table 'Notatki'
ALTER TABLE [dbo].[Notatki]
ADD CONSTRAINT [FK_UzytkownicyNotatki]
    FOREIGN KEY ([UzytkownikId])
    REFERENCES [dbo].[Uzytkownicy]
        ([UzytkownikId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UzytkownicyNotatki'
CREATE INDEX [IX_FK_UzytkownicyNotatki]
ON [dbo].[Notatki]
    ([UzytkownikId]);
GO

-- Creating foreign key on [UzytkownikId] in table 'Spotkanies'
ALTER TABLE [dbo].[Spotkanies]
ADD CONSTRAINT [FK_UzytkownicySpotkanies]
    FOREIGN KEY ([UzytkownikId])
    REFERENCES [dbo].[Uzytkownicy]
        ([UzytkownikId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UzytkownicySpotkanies'
CREATE INDEX [IX_FK_UzytkownicySpotkanies]
ON [dbo].[Spotkanies]
    ([UzytkownikId]);
GO

-- Creating foreign key on [RodzajAdresuID] in table 'Adresy'
ALTER TABLE [dbo].[Adresy]
ADD CONSTRAINT [FK_RodzajAdresuAdresy]
    FOREIGN KEY ([RodzajAdresuID])
    REFERENCES [dbo].[RodzajAdresu]
        ([RodzajAdresuID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RodzajAdresuAdresy'
CREATE INDEX [IX_FK_RodzajAdresuAdresy]
ON [dbo].[Adresy]
    ([RodzajAdresuID]);
GO

-- Creating foreign key on [Status_Klienta_Id] in table 'Klienci'
ALTER TABLE [dbo].[Klienci]
ADD CONSTRAINT [FK_StatusKlientaKlienci]
    FOREIGN KEY ([Status_Klienta_Id])
    REFERENCES [dbo].[StatusKlienta]
        ([StatusId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_StatusKlientaKlienci'
CREATE INDEX [IX_FK_StatusKlientaKlienci]
ON [dbo].[Klienci]
    ([Status_Klienta_Id]);
GO

-- Creating foreign key on [WojewodztwoId] in table 'Adresy'
ALTER TABLE [dbo].[Adresy]
ADD CONSTRAINT [FK_WojewodztwaAdresy]
    FOREIGN KEY ([WojewodztwoId])
    REFERENCES [dbo].[Wojewodztwa]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_WojewodztwaAdresy'
CREATE INDEX [IX_FK_WojewodztwaAdresy]
ON [dbo].[Adresy]
    ([WojewodztwoId]);
GO

-- Creating foreign key on [OsobaId] in table 'Uzytkownicy'
ALTER TABLE [dbo].[Uzytkownicy]
ADD CONSTRAINT [FK_OsobaUzytkownicy]
    FOREIGN KEY ([OsobaId])
    REFERENCES [dbo].[Osoba]
        ([OsobaId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OsobaUzytkownicy'
CREATE INDEX [IX_FK_OsobaUzytkownicy]
ON [dbo].[Uzytkownicy]
    ([OsobaId]);
GO

-- Creating foreign key on [Branza_Id] in table 'Klienci'
ALTER TABLE [dbo].[Klienci]
ADD CONSTRAINT [FK_BranzaKlientaKlienci]
    FOREIGN KEY ([Branza_Id])
    REFERENCES [dbo].[BranzaKlienta]
        ([BranzaID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BranzaKlientaKlienci'
CREATE INDEX [IX_FK_BranzaKlientaKlienci]
ON [dbo].[Klienci]
    ([Branza_Id]);
GO

-- Creating foreign key on [PanstwoId] in table 'Adresy'
ALTER TABLE [dbo].[Adresy]
ADD CONSTRAINT [FK_PanstwaAdresy]
    FOREIGN KEY ([PanstwoId])
    REFERENCES [dbo].[Panstwa]
        ([PanstwoID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PanstwaAdresy'
CREATE INDEX [IX_FK_PanstwaAdresy]
ON [dbo].[Adresy]
    ([PanstwoId]);
GO

-- Creating foreign key on [UzytkownikId] in table 'Klienci'
ALTER TABLE [dbo].[Klienci]
ADD CONSTRAINT [FK_UzytkownicyKlienci]
    FOREIGN KEY ([UzytkownikId])
    REFERENCES [dbo].[Uzytkownicy]
        ([UzytkownikId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UzytkownicyKlienci'
CREATE INDEX [IX_FK_UzytkownicyKlienci]
ON [dbo].[Klienci]
    ([UzytkownikId]);
GO

-- Creating foreign key on [UzytkownikId] in table 'Zadania'
ALTER TABLE [dbo].[Zadania]
ADD CONSTRAINT [FK_UzytkownicyZadania]
    FOREIGN KEY ([UzytkownikId])
    REFERENCES [dbo].[Uzytkownicy]
        ([UzytkownikId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UzytkownicyZadania'
CREATE INDEX [IX_FK_UzytkownicyZadania]
ON [dbo].[Zadania]
    ([UzytkownikId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------