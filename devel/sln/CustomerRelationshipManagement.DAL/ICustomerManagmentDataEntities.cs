﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
namespace CustomerRelationshipManagement.DAL
{
    public interface ICustomerManagmentDataEntities
    {
        DbSet<Adresy> Adresy { get; set; }
        DbSet<Hasla> Hasla { get; set; }
        DbSet<Historia> Historia { get; set; }
        DbSet<Klienci> Klienci { get; set; }
        DbSet<Notatki> Notatki { get; set; }
        DbSet<Osoba> Osoba { get; set; }
        DbSet<Spotkanies> Spotkanies { get; set; }
        DbSet<sysdiagrams> sysdiagrams { get; set; }
        DbSet<Uzytkownicy> Uzytkownicy { get; set; }
        DbSet<Wojewodztwa> Wojewodztwa { get; set; }
        DbSet<NotatkiUzytkownikow> NotatkiUzytkownikow { get; set; }
        DbSet<RodzajAdresu> RodzajAdresu { get; set; }
        DbSet<Panstwa> Panstwa { get; set; }
        DbSet<BranzaKlienta> BranzaKlienta { get; set; }
        DbSet<StatusKlienta> StatusKlienta { get; set; }
        DbSet<AdresyKlientow> AdresyKlientow { get; set; }

        void Save();
        DbEntityEntry<T> Entry<T>(T enity) where T : class;

    }
}
