﻿using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoC
{
    public class IocContainer 
    {
        private static IocContainer _instance;
        public static IocContainer Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new IocContainer();
                }
                return _instance;
            }
        }

        private IWindsorContainer _container;

        public IWindsorContainer Container { get { return _container; } }

        public IocContainer()
        {
            _container = new WindsorContainer();
            _container.AddFacility<WcfFacility>();
        }

        public void Register(Type specyfication, Type implementation)
        {
            _container.Register(Component.For(specyfication).ImplementedBy(implementation));
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public void InstallFromXml(string name)
        {
            _container =  _container.Install(Configuration.FromXmlFile(name));
        }
       
    }
}
