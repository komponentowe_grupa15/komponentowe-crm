﻿using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;


namespace CustomService
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            //IoC.IocContainer.Instance.Container.Register(Component.For<ICustomerManagmentDataEntities>().ImplementedBy<CustomerManagmentDataEntities2>().LifeStyle.Transient);
            //IoC.IocContainer.Instance.InstallFromXml("Windsor.config");
             
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            var data = DateTime.Now;
        }
    }
}