﻿namespace CRM
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TabPage OrganizerTab;
            this.MainMenu = new System.Windows.Forms.TabControl();
            this.CustomerManagementTab = new System.Windows.Forms.TabPage();
            this.ProductsManagementTab = new System.Windows.Forms.TabPage();
            this.RaportsTab = new System.Windows.Forms.TabPage();
            this.Authors = new System.Windows.Forms.TabPage();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.LoginStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            OrganizerTab = new System.Windows.Forms.TabPage();
            this.MainMenu.SuspendLayout();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // OrganizerTab
            // 
            OrganizerTab.BackColor = System.Drawing.Color.PaleGreen;
            OrganizerTab.Location = new System.Drawing.Point(4, 34);
            OrganizerTab.Name = "OrganizerTab";
            OrganizerTab.Padding = new System.Windows.Forms.Padding(3);
            OrganizerTab.Size = new System.Drawing.Size(967, 495);
            OrganizerTab.TabIndex = 0;
            OrganizerTab.Text = "Organizer";
            // 
            // MainMenu
            // 
            this.MainMenu.Controls.Add(OrganizerTab);
            this.MainMenu.Controls.Add(this.CustomerManagementTab);
            this.MainMenu.Controls.Add(this.ProductsManagementTab);
            this.MainMenu.Controls.Add(this.RaportsTab);
            this.MainMenu.Controls.Add(this.Authors);
            this.MainMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MainMenu.ItemSize = new System.Drawing.Size(180, 30);
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Multiline = true;
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.SelectedIndex = 0;
            this.MainMenu.Size = new System.Drawing.Size(975, 533);
            this.MainMenu.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.MainMenu.TabIndex = 1;
            // 
            // CustomerManagementTab
            // 
            this.CustomerManagementTab.Location = new System.Drawing.Point(4, 34);
            this.CustomerManagementTab.Name = "CustomerManagementTab";
            this.CustomerManagementTab.Padding = new System.Windows.Forms.Padding(3);
            this.CustomerManagementTab.Size = new System.Drawing.Size(967, 495);
            this.CustomerManagementTab.TabIndex = 1;
            this.CustomerManagementTab.Text = "Zarządzanie klientami";
            this.CustomerManagementTab.UseVisualStyleBackColor = true;
            // 
            // ProductsManagementTab
            // 
            this.ProductsManagementTab.Location = new System.Drawing.Point(4, 34);
            this.ProductsManagementTab.Name = "ProductsManagementTab";
            this.ProductsManagementTab.Size = new System.Drawing.Size(967, 495);
            this.ProductsManagementTab.TabIndex = 2;
            this.ProductsManagementTab.Text = "Zarządzanie Produktami";
            this.ProductsManagementTab.UseVisualStyleBackColor = true;
            // 
            // RaportsTab
            // 
            this.RaportsTab.Location = new System.Drawing.Point(4, 34);
            this.RaportsTab.Name = "RaportsTab";
            this.RaportsTab.Size = new System.Drawing.Size(967, 495);
            this.RaportsTab.TabIndex = 3;
            this.RaportsTab.Text = "Raporty";
            this.RaportsTab.UseVisualStyleBackColor = true;
            // 
            // Authors
            // 
            this.Authors.Location = new System.Drawing.Point(4, 34);
            this.Authors.Name = "Authors";
            this.Authors.Size = new System.Drawing.Size(967, 495);
            this.Authors.TabIndex = 4;
            this.Authors.Text = "Autorzy";
            this.Authors.UseVisualStyleBackColor = true;
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LoginStatusLabel});
            this.StatusStrip.Location = new System.Drawing.Point(0, 511);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(975, 22);
            this.StatusStrip.TabIndex = 2;
            this.StatusStrip.Text = "statusStrip1";
            // 
            // LoginStatusLabel
            // 
            this.LoginStatusLabel.Name = "LoginStatusLabel";
            this.LoginStatusLabel.Size = new System.Drawing.Size(126, 17);
            this.LoginStatusLabel.Text = "Nie jesteś zalogowany!";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(975, 533);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.MainMenu);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CRM";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MainMenu.ResumeLayout(false);
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl MainMenu;
        private System.Windows.Forms.TabPage CustomerManagementTab;
        private System.Windows.Forms.TabPage ProductsManagementTab;
        private System.Windows.Forms.TabPage RaportsTab;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel LoginStatusLabel;
        private System.Windows.Forms.TabPage Authors;
    }
}

