﻿using Calendar.NET;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using CRM.forms;
using CustomService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using CustomerRelationshipManagement.DAL;
using System.IO;

namespace CRM
{
    public partial class MainForm : Form
    {

        ClientServiceWrapper.MainManager _services = ClientServiceWrapper.MainManager.Instance;
        List<ShopManager.Common.Contracts.Product> products;
        public SzczegolyKlienta szczegolyKlienta = new SzczegolyKlienta();

        public MainForm()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(x => _services.CheckConncetion()));
            InitializeComponent();
            calendar.CalendarDate = DateTime.Now;
            calendar.CalendarView = CalendarViews.Month;


        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.Visible = false;
            LoginForm form = new LoginForm(this);
            bool loginSuccessfully = false;

            while (!loginSuccessfully)
            {
                if (form.ShowDialog() == DialogResult.OK)
                {

                    TextBox loginTxBox = (TextBox)form.Controls["loginTxBox"];
                    TextBox passTxBox = (TextBox)form.Controls["passwordTxBox"];

                    if (_services.CustomServiceInstance.ValidateLogin(loginTxBox.Text, passTxBox.Text))
                    {

                        ClientServiceWrapper.MainManager.Instance.UserInfo = _services.CustomServiceInstance.PobierzUzytkownika(loginTxBox.Text);

                        timer1.Start();
                        ThreadPool.QueueUserWorkItem(
                            new WaitCallback(a => this.UpdateDateGridViewForClients()
                                ));
                        ThreadPool.QueueUserWorkItem(new WaitCallback(a => this.ClearCalendar()));
                        ThreadPool.QueueUserWorkItem(new WaitCallback(a => this.UpdateTaskList()));
                        ThreadPool.QueueUserWorkItem(new WaitCallback(a => this.UpdateNotatki()));


                        loginSuccessfully = true;
                        this.Text += " - Jesteś zalogowany jako: " + _services.UserInfo.Login;
                        this.Visible = true;
                        ThreadPool.QueueUserWorkItem(new WaitCallback(a =>
                        {
                            //_services.CacheManager.Add<List<string>>("lista_adresowa", _services.CustomServiceInstance.PobierzUzytkownikow());
                            //var lista_adresowa = _services.CacheManager.GetValue<List<string>>("lista_adresowa");
                            //if (lista_adresowa == null)
                            //{
                            //    lista_adresowa = new List<string>();
                            //}
                            ////a.Tables["Pracownicy"].Rows[0].ItemArray[1] + " " + a.Tables["Pracownicy"].Rows[0].ItemArray[2]
                            //var rows = _services.Pracownicy.ZwrocPracownikow().Tables["Pracownicy"].Rows;
                            //foreach (DataRow item in rows)
                            //{
                            //    lista_adresowa.Add(item[1] + " " + item[2]);
                            //}
                            //_services.CacheManager.Add<List<string>>("lista_adresowa", lista_adresowa);
                            //var listaAdresowa = _services.CacheManager.GetValue<List<string>>("lista_adresowa").ToList();
                            //ListaKlientow.Invoke(new Action(() => ListaKlientow.Items.AddRange(listaAdresowa.Where(x => string.Compare(x, _services.CustomServiceInstance.PobierzImieINazwisko(_services.UserInfo.UzytkownikId)) != 0)
                            //    .ToArray())));
                        }));

                    }
                    else
                    {
                        MessageBox.Show("Nieprawidłowa nazwa użytkownika lub hasło!", "Błąd logowania", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else Environment.Exit(0);
            }
        }

        private void MainMenu_SizeChanged(object sender, EventArgs e)
        {
            klienciDgv.Size = new Size(MainMenu.Size.Width, MainMenu.Size.Height - 90 - 23);
        }

        private void klienciDgv_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
        }


        private void klienciDgv_Scroll(object sender, ScrollEventArgs e)
        {
            //filtrDgv.VerticalScrollingOffset = klienciDgv.VerticalScrollingOffset;

        }

        private void calendar_Load(object sender, EventArgs e)
        {

        }

        private void calendar_Click(object sender, EventArgs e)
        {

        }

        private void szczegolyBtn_Click(object sender, EventArgs e)
        {

            Klienci klient = _services.Klienci.ZnajdzKlientow(ClientServiceWrapper.MainManager.Instance.UserInfo.UzytkownikId).Where(x => x.KlientId == int.Parse(klienciDgv[2, klienciDgv.SelectedRows[0].Index].Value.ToString())).First();

            szczegolyKlienta = new SzczegolyKlienta(klient, this);
            szczegolyKlienta.Show();
        }

        private void UsunBtn_Click(object sender, EventArgs e)
        {
            if (ListaZadanListBox.SelectedIndex != -1)
            {
                var item = ListaZadanListBox.Items[ListaZadanListBox.SelectedIndex];
                _services.Organizer.UsunZadanie((string)item, _services.UserInfo.UzytkownikId);
                ThreadPool.QueueUserWorkItem(new WaitCallback(x =>
                {
                    _services.AddLog(new Historia()
                    {
                        Akcja = "Usnięto notatke : " + item,
                        UzytkownikID = _services.UserInfo.UzytkownikId,
                        Data = DateTime.Now
                    });
                    this.UpdateTaskList();
                }));
            }

        }

        private void DodajBtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(PoleEdycjiTxBox.Text))
            {

                ThreadPool.QueueUserWorkItem(new WaitCallback(x =>
                {
                    int id = _services.Organizer.DodajZadanie(new Zadania()
                        {
                            Data = DateTime.Now,
                            Priorytet = 1,
                            Tekst = PoleEdycjiTxBox.Text,
                            UzytkownikId = _services.UserInfo.UzytkownikId
                        });
                    _services.AddLog(new Historia()
                    {
                        Data = DateTime.Now,
                        Akcja = string.Format("Dodano notatkę, dla userId = {0}, o id {1}", _services.UserInfo.UzytkownikId, id),
                        UzytkownikID = _services.UserInfo.UzytkownikId

                    });
                    this.UpdateTaskList();
                }));

            }

        }

        private void DodajKlientaBtn_Click(object sender, EventArgs e)
        {
            //SzczegolyKlienta 
            szczegolyKlienta = new SzczegolyKlienta(null, this);
            szczegolyKlienta.Show();
        }

        private void logoutLbl_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dodajSpotkanieBtn_Click(object sender, EventArgs e)
        {
            DodajDoKalendarzaForm dodaj = new DodajDoKalendarzaForm(calendar);
            dodaj.ShowDialog();

            
        }

        private void RightClickMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

            ToolStripMenuItem t = (ToolStripMenuItem)(e.ClickedItem);
            klienciDgv.Columns[t.Text].Visible = !t.Checked;
        }


        private void NotesTxBox_Click(object sender, EventArgs e)
        {

            if (NotesTxBox.Text == "Dodaj notatkę...")
            {
                NotesTxBox.Text = string.Empty;
            }
        }



        private void filtrDgv_CellEnter(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void klienciDgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0) //edycja
            {
                Klienci klient = _services.Klienci.ZnajdzKlientow(ClientServiceWrapper.MainManager.Instance.UserInfo.UzytkownikId).Where(x => x.KlientId == int.Parse(klienciDgv[2, e.RowIndex].Value.ToString())).First();
                //(Klienci)klienciDgv.Rows[e.RowIndex].DataBoundItem;
                szczegolyKlienta = new SzczegolyKlienta(klient, this);
                szczegolyKlienta.Show();
            }
            else if (e.ColumnIndex == 1) //usuwanie
            {
                if (MessageBox.Show("Czy jesteś tego pewien?", "Ostrzeżenie", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    Klienci klient = _services.Klienci.ZnajdzKlientow(ClientServiceWrapper.MainManager.Instance.UserInfo.UzytkownikId).Where(x => x.KlientId == int.Parse(klienciDgv[2, e.RowIndex].Value.ToString())).First();
                    ClientServiceWrapper.MainManager.Instance.Klienci.UsunKlienta(klient.KlientId);
                    ClientServiceWrapper.MainManager.Instance.Klienci.UsunPrzypisaniePoIdKlienta(klient.KlientId);
                    ThreadPool.QueueUserWorkItem(
                            new WaitCallback(a => this.UpdateDateGridViewForClients()
                                ));
                }
            }
        }

        public void UpdateDateGridViewForClients(string name = null)
        {
            var userId = ClientServiceWrapper.MainManager.Instance.UserInfo.UzytkownikId;
            var source = new BindingSource();
            _services.CacheManager.Add<List<Klienci>>("klienci", _services.Klienci.ZnajdzKlientow(userId));
            source.DataSource = _services.CacheManager.GetValue<List<Klienci>>("klienci").Where(x => name != null ? x.Nazwa_Klienta.ToLowerInvariant().Contains(name.ToLowerInvariant()) : true).Select(x => new
            {
                x.KlientId,
                x.Nazwa_Klienta,
                x.BranzaKlienta.Branza,
                x.NIP,
                x.REGON,
                x.Strona_WWW,
                x.StatusKlienta.Status,
                x.Uwagi
            });

            if (klienciDgv.InvokeRequired)
            {
                klienciDgv.Invoke(new Action(()
                    => klienciDgv.DataSource = source));
            }
            else
            {
                klienciDgv.DataSource = source;
            }
            //prawoklik
            if (RightClickMenu.InvokeRequired)
            {
                RightClickMenu.Invoke(new Action(() => RightClickMenu.Items.Clear()));
            }
            else { RightClickMenu.Items.Clear(); }

            foreach (DataGridViewColumn item in klienciDgv.Columns)
            {


                if (RightClickMenu.InvokeRequired)
                {
                    RightClickMenu.Invoke(new Action(() => RightClickMenu.Items.Add(item.Name)));
                }
                else { RightClickMenu.Items.Add(item.Name); }
                //RightClickMenu.Items.Add(item.Name);

                if (klienciDgv.InvokeRequired)
                {
                    klienciDgv.Invoke(new
                    Action(() => klienciDgv.Columns[item.Name].Visible = true));
                }
                item.Visible = true;

            }



            if (RightClickMenu.InvokeRequired)
            {
                RightClickMenu.Invoke(new Action(() =>
                {
                    RightClickMenu.Items.RemoveAt(0);
                    RightClickMenu.Items.RemoveAt(0);
                    foreach (ToolStripMenuItem item in RightClickMenu.Items)
                    {
                        item.Checked = true;
                        item.CheckOnClick = true;
                    }
                }));
            }
            else
            {
                RightClickMenu.Items.RemoveAt(0);
                RightClickMenu.Items.RemoveAt(0);
                foreach (ToolStripMenuItem item in RightClickMenu.Items)
                {
                    item.Checked = true;
                    item.CheckOnClick = true;
                }
            }

        }

        public void ClearCalendar()
        {
            var events = _services.Organizer.BeginWcfCall(x => x.PobierzSpotkania(_services.UserInfo.UzytkownikId));
            if (calendar.InvokeRequired)
            {
                calendar.Invoke(new Action(() => calendar.RemoveAllEvents()));
                AddCalendarEvents(events);
            }
            else
            {
                calendar.RemoveAllEvents();
                AddCalendarEvents(events);

            }

        }

        public void UpdateTaskList()
        {
            if (ListaZadanListBox.InvokeRequired)
            {
                ListaZadanListBox.Invoke(new Action(() =>
                {
                    ListaZadanListBox.DataSource = _services.Organizer.PobierzZadania(_services.UserInfo.UzytkownikId).Select(x => x.Tekst).ToList();

                }));
            }
            else
            {
                ListaZadanListBox.DataSource = _services.Organizer.PobierzZadania(_services.UserInfo.UzytkownikId);
            }
        }

        public void UpdateNotatki()
        {
            if (NotesTxBox.InvokeRequired)
            {
                NotesTxBox.Invoke(new Action(() =>
                {
                    string notka = _services.Organizer.PobierzNotatki(_services.UserInfo.UzytkownikId).Select(x => x.Notatka).FirstOrDefault();
                    if (!string.IsNullOrEmpty(notka))
                    {
                        NotesTxBox.Text = notka;
                    }


                }));
            }
            else
            {
                string notka = _services.Organizer.PobierzNotatki(_services.UserInfo.UzytkownikId).Select(x => x.Notatka).FirstOrDefault();
                if (!string.IsNullOrEmpty(notka))
                {
                    NotesTxBox.Text = notka;
                }
            }
        }

        private void AddCalendarEvents(IWcfAsyncCall<List<Spotkanies>> events)
        {
            var foundedEvents = events.End();
            foundedEvents.ForEach(x => calendar.AddEvent(new CustomEvent()
            {
                Date = x.Data,
                EventText = x.Nazwa,
                EventLengthInHours = (float)x.DlugoscTrwania
            }));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClearCalendar();
        }

        private void EksportBtn_Click(object sender, EventArgs e)
        {
            var dt = klienciDgv.DataSource as List<Klienci>;
            saveFD.Filter = "Plik Excel (.xlsx)|*.xlsx|Plik CSV (.csv)|*.csv";
            if (saveFD.ShowDialog() == DialogResult.OK)
            {
                FileInfo plik = new FileInfo(saveFD.FileName);
                byte[] fileStream = ClientServiceWrapper.MainManager.Instance.ImportEksportKontaktow.Eksportuj(ClientServiceWrapper.MainManager.Instance.UserInfo.UzytkownikId, plik.Extension);
                try
                {
                    File.WriteAllBytes(saveFD.FileName, fileStream);
                }
                catch (Exception)
                {
                    MessageBox.Show("Nie można zapisać pliku!", "Błąd zapisu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }

        }

        private void filtrDgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show("Test" + e.ColumnIndex + e.RowIndex);
        }


        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(SearchProductBox.Text) || SearchProductBox.Text == "Podaj nazwę...")
            {

                products = ClientServiceWrapper.MainManager.Instance.Products.GetProducts(new ShopManager.Common.Contracts.Filter() { Count = 100 }).Items.ToList();
                productsGrid.DataSource = GetProductDataSource(products);
            }
            else
            {
                if (products != null)
                {
                    products = ClientServiceWrapper.MainManager.Instance.Products.GetProducts(new ShopManager.Common.Contracts.Filter() { Count = 100 }).Items.ToList();
                    var foundedProducts = products.Where(x => x.Name.ToLowerInvariant().Contains(SearchProductBox.Text.ToLowerInvariant())).ToList();
                    if (foundedProducts.Count == 0)
                    {
                        MessageBox.Show("Nie znaleziono produktu, którego nazwa spełniałaby kryteria wyszukiwania.");
                    }
                    productsGrid.DataSource = GetProductDataSource(foundedProducts);
                }

            }
        }

        private IList<object> GetProductDataSource(IEnumerable<ShopManager.Common.Contracts.Product> products)
        {
            return products.Select(x => new
               {
                   Id = x.Id,
                   Nazwa = x.Name,
                   Cena = x.Price,
                   Vat = x.Vat.Value,
                   Dostepny = x.Available,
                   OstatniaModyfikacja = x.Updated
               }).ToList<object>();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            var list = ClientServiceWrapper.MainManager.Instance.GetLogs();
            if (list.Count() != 0)
            {
                _services.Logger.Log(list);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(x =>
            logGridView.Invoke(new Action(()
                => logGridView.DataSource = _services.GetLogs().Select(y =>
                new
                {
                    Akcja = y.Akcja,
                    UzytkownikID = y.UzytkownikID,
                    Data = y.Data
                })
                ))));
        }

        private void searchClientsBtn_Click(object sender, EventArgs e)
        {
            var text = searchClientsBox.Text;
            var klienci = _services.CacheManager.GetValue<List<Klienci>>("klienci");
            if (!string.IsNullOrEmpty(text))
            {
                klienciDgv.DataSource = klienci.Where(x => x.Nazwa_Klienta.ToLowerInvariant().Contains(text)).Select(x => new
                {
                    x.KlientId,
                    x.Nazwa_Klienta,
                    x.BranzaKlienta.Branza,
                    x.NIP,
                    x.REGON,
                    x.Strona_WWW,
                    x.StatusKlienta.Status,
                    x.Uwagi
                }).ToList();

            }
            else
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(x => this.UpdateDateGridViewForClients()));
            }
        }





        private void SearchProductBox_Click(object sender, EventArgs e)
        {
            if (searchClientsBox.Text == "Podaj nazwę...")
            {
                SearchProductBox.Text = string.Empty;
            }

        }



        private void szukajkaKlientow_Click(object sender, EventArgs e)
        {
            if (szukajkaKlientow.Text == "Szukaj pracownika...")
            {
                szukajkaKlientow.Text = string.Empty;
            }
        }

        private void aktualizujNotatke_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(NotesTxBox.Text) || NotesTxBox.Text == "Dodaj notatkę...")
            {
                MessageBox.Show("Dodaj coś, aby zapisać notatkę...", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                Notatki notka = new Notatki();
                notka.Data_dodania = DateTime.Now;
                notka.Tytul = "";
                notka.UzytkownikId = _services.UserInfo.UzytkownikId;
                notka.Notatka = NotesTxBox.Text;
                _services.Organizer.DodajNotatke(notka);
            }
        }

        private void NotesTxBox_TextChanged(object sender, EventArgs e)
        {
            NotesGrBox.Text = "Notes - Pozostało " + (2000 - NotesTxBox.Text.Length) + " znaków";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
                var row = _services.Pracownicy.ZwrocPracownikow().Tables["Pracownicy"].Rows;
                ListaKlientow.Items.Clear();
                if (szukajkaKlientow.Text != null)
                {
                    foreach (DataRow item in row)
                    {
                        if ((item[1] + " " + item[2]).Contains(szukajkaKlientow.Text))
                        {
                            ListaKlientow.Items.Add(item[1] + " " + item[2]);
                        }
                    }
                }
                else
                {

                    foreach (DataRow item in row)
                    {
                        ListaKlientow.Items.Add(item[1] + " " + item[2]);
                    }
                }
                _services.AddLog(new Historia() { Akcja = "Wyswietlam pracownikow dla tekstu " + szukajkaKlientow.Text, Data = DateTime.Now, UzytkownikID = _services.UserInfo.UzytkownikId });
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<Wiadomosci> wiad = new List<Wiadomosci>();
            var user = _services.CustomServiceInstance.PobierzImieINazwisko(_services.UserInfo.UzytkownikId);
            if (!string.IsNullOrEmpty(wiadomoscTxtbx.Text) && ListaKlientow.CheckedItems.Count != 0)
            {
                foreach (var item in ListaKlientow.CheckedItems)
                {
                    wiad.Add(new Wiadomosci()
                    {
                        Data = DateTime.Now,
                        DoKogo = item.ToString(),
                        Temat = "Wiadomosc",
                        Tresc = wiadomoscTxtbx.Text,
                        OdKogo = user
                    }
                         );
                }
            }
            else
            {
                MessageBox.Show("Tekst wiadomości nie może być pusty, musis również zaznaczyć conajmniej jednego pracownika");
            }
            _services.MailingService.WyslijWiadomosci(wiad);
            MessageBox.Show("Wysłano wiadomość !");
        }


    }
}

