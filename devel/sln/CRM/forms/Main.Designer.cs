﻿namespace CRM
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TabPage OrganizerTab;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.organizerTable = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.calendar = new Calendar.NET.Calendar();
            this.dodajSpotkanieBtn = new System.Windows.Forms.Button();
            this.NotesGrBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.NotesTxBox = new System.Windows.Forms.TextBox();
            this.aktualizujNotatke = new System.Windows.Forms.Button();
            this.ListaZadanGrBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.ListaZadanListBox = new System.Windows.Forms.ListBox();
            this.PoleEdycjiTxBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.DodajBtn = new System.Windows.Forms.Button();
            this.UsunBtn = new System.Windows.Forms.Button();
            this.MainMenu = new System.Windows.Forms.TabControl();
            this.zarzadzanieKlientamiTab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.szczegolyBtn = new System.Windows.Forms.Button();
            this.DodajKlientaBtn = new System.Windows.Forms.Button();
            this.EksportBtn = new System.Windows.Forms.Button();
            this.WyczyscFiltryBtn = new System.Windows.Forms.Button();
            this.searchClientsBtn = new System.Windows.Forms.Button();
            this.searchClientsBox = new System.Windows.Forms.RichTextBox();
            this.klienciDgv = new System.Windows.Forms.DataGridView();
            this.Edytuj = new System.Windows.Forms.DataGridViewImageColumn();
            this.Usuń = new System.Windows.Forms.DataGridViewImageColumn();
            this.RightClickMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.zarzadzanieProduktamiTab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.productsGrid = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.SearchProductBox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.mailingTab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.ListaKlientow = new System.Windows.Forms.CheckedListBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.szukajkaKlientow = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.LogTab = new System.Windows.Forms.TabPage();
            this.logGridView = new System.Windows.Forms.DataGridView();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WyczyscCol = new System.Windows.Forms.DataGridViewButtonColumn();
            this.saveFD = new System.Windows.Forms.SaveFileDialog();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn4 = new System.Windows.Forms.DataGridViewImageColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.wiadomoscTxtbx = new System.Windows.Forms.RichTextBox();
            this.button3 = new System.Windows.Forms.Button();
            OrganizerTab = new System.Windows.Forms.TabPage();
            OrganizerTab.SuspendLayout();
            this.organizerTable.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.NotesGrBox.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.ListaZadanGrBox.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.zarzadzanieKlientamiTab.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.klienciDgv)).BeginInit();
            this.zarzadzanieProduktamiTab.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productsGrid)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.mailingTab.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.LogTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logGridView)).BeginInit();
            this.tableLayoutPanel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // OrganizerTab
            // 
            OrganizerTab.BackColor = System.Drawing.Color.WhiteSmoke;
            OrganizerTab.Controls.Add(this.organizerTable);
            OrganizerTab.Location = new System.Drawing.Point(4, 34);
            OrganizerTab.Name = "OrganizerTab";
            OrganizerTab.Padding = new System.Windows.Forms.Padding(3);
            OrganizerTab.Size = new System.Drawing.Size(1139, 574);
            OrganizerTab.TabIndex = 0;
            OrganizerTab.Text = "Organizer";
            // 
            // organizerTable
            // 
            this.organizerTable.ColumnCount = 2;
            this.organizerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.42983F));
            this.organizerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.57017F));
            this.organizerTable.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.organizerTable.Controls.Add(this.ListaZadanGrBox, 1, 0);
            this.organizerTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.organizerTable.Location = new System.Drawing.Point(3, 3);
            this.organizerTable.Name = "organizerTable";
            this.organizerTable.RowCount = 1;
            this.organizerTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.organizerTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 568F));
            this.organizerTable.Size = new System.Drawing.Size(1133, 568);
            this.organizerTable.TabIndex = 3;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.NotesGrBox, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 443F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(689, 562);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(683, 437);
            this.panel1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.calendar);
            this.groupBox1.Controls.Add(this.dodajSpotkanieBtn);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(683, 437);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kalendarz";
            // 
            // calendar
            // 
            this.calendar.AllowEditingEvents = true;
            this.calendar.CalendarDate = new System.DateTime(2013, 1, 1, 23, 30, 33, 659);
            this.calendar.CalendarView = Calendar.NET.CalendarViews.Month;
            this.calendar.DateHeaderFont = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.calendar.DayOfWeekFont = new System.Drawing.Font("Arial", 10F);
            this.calendar.DaysFont = new System.Drawing.Font("Arial", 10F);
            this.calendar.DayViewTimeFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.calendar.DimDisabledEvents = true;
            this.calendar.HighlightCurrentDay = true;
            this.calendar.LoadPresetHolidays = false;
            this.calendar.Location = new System.Drawing.Point(6, 22);
            this.calendar.Name = "calendar";
            this.calendar.ShowArrowControls = true;
            this.calendar.ShowDashedBorderOnDisabledEvents = true;
            this.calendar.ShowDateInHeader = true;
            this.calendar.ShowDisabledEvents = false;
            this.calendar.ShowEventTooltips = true;
            this.calendar.ShowTodayButton = true;
            this.calendar.Size = new System.Drawing.Size(677, 373);
            this.calendar.TabIndex = 1;
            this.calendar.TodayFont = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.calendar.Click += new System.EventHandler(this.calendar_Click);
            // 
            // dodajSpotkanieBtn
            // 
            this.dodajSpotkanieBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dodajSpotkanieBtn.Location = new System.Drawing.Point(6, 395);
            this.dodajSpotkanieBtn.Name = "dodajSpotkanieBtn";
            this.dodajSpotkanieBtn.Size = new System.Drawing.Size(164, 30);
            this.dodajSpotkanieBtn.TabIndex = 0;
            this.dodajSpotkanieBtn.Text = "Dodaj do kalendarza";
            this.dodajSpotkanieBtn.UseVisualStyleBackColor = true;
            this.dodajSpotkanieBtn.Click += new System.EventHandler(this.dodajSpotkanieBtn_Click);
            // 
            // NotesGrBox
            // 
            this.NotesGrBox.Controls.Add(this.tableLayoutPanel8);
            this.NotesGrBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NotesGrBox.Location = new System.Drawing.Point(3, 446);
            this.NotesGrBox.Name = "NotesGrBox";
            this.NotesGrBox.Size = new System.Drawing.Size(683, 113);
            this.NotesGrBox.TabIndex = 2;
            this.NotesGrBox.TabStop = false;
            this.NotesGrBox.Text = "Notes";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97F));
            this.tableLayoutPanel8.Controls.Add(this.NotesTxBox, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.aktualizujNotatke, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 19);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(677, 91);
            this.tableLayoutPanel8.TabIndex = 3;
            // 
            // NotesTxBox
            // 
            this.NotesTxBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NotesTxBox.Location = new System.Drawing.Point(3, 3);
            this.NotesTxBox.Multiline = true;
            this.NotesTxBox.Name = "NotesTxBox";
            this.NotesTxBox.Size = new System.Drawing.Size(574, 85);
            this.NotesTxBox.TabIndex = 2;
            this.NotesTxBox.Text = "Dodaj notatkę...";
            this.NotesTxBox.Click += new System.EventHandler(this.NotesTxBox_Click);
            this.NotesTxBox.TextChanged += new System.EventHandler(this.NotesTxBox_TextChanged);
            // 
            // aktualizujNotatke
            // 
            this.aktualizujNotatke.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.aktualizujNotatke.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aktualizujNotatke.Location = new System.Drawing.Point(583, 60);
            this.aktualizujNotatke.Name = "aktualizujNotatke";
            this.aktualizujNotatke.Size = new System.Drawing.Size(91, 28);
            this.aktualizujNotatke.TabIndex = 3;
            this.aktualizujNotatke.Text = "Aktualizuj";
            this.aktualizujNotatke.UseVisualStyleBackColor = true;
            this.aktualizujNotatke.Click += new System.EventHandler(this.aktualizujNotatke_Click);
            // 
            // ListaZadanGrBox
            // 
            this.ListaZadanGrBox.Controls.Add(this.tableLayoutPanel5);
            this.ListaZadanGrBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListaZadanGrBox.Location = new System.Drawing.Point(698, 3);
            this.ListaZadanGrBox.Name = "ListaZadanGrBox";
            this.ListaZadanGrBox.Size = new System.Drawing.Size(432, 562);
            this.ListaZadanGrBox.TabIndex = 2;
            this.ListaZadanGrBox.TabStop = false;
            this.ListaZadanGrBox.Text = "Lista zadań";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.ListaZadanListBox, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.PoleEdycjiTxBox, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel7, 0, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 19);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(426, 540);
            this.tableLayoutPanel5.TabIndex = 3;
            // 
            // ListaZadanListBox
            // 
            this.ListaZadanListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListaZadanListBox.FormattingEnabled = true;
            this.ListaZadanListBox.HorizontalScrollbar = true;
            this.ListaZadanListBox.ItemHeight = 16;
            this.ListaZadanListBox.Location = new System.Drawing.Point(3, 3);
            this.ListaZadanListBox.Name = "ListaZadanListBox";
            this.ListaZadanListBox.Size = new System.Drawing.Size(420, 465);
            this.ListaZadanListBox.TabIndex = 0;
            // 
            // PoleEdycjiTxBox
            // 
            this.PoleEdycjiTxBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PoleEdycjiTxBox.Location = new System.Drawing.Point(3, 474);
            this.PoleEdycjiTxBox.Name = "PoleEdycjiTxBox";
            this.PoleEdycjiTxBox.Size = new System.Drawing.Size(420, 23);
            this.PoleEdycjiTxBox.TabIndex = 2;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.DodajBtn, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.UsunBtn, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 503);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(420, 34);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // DodajBtn
            // 
            this.DodajBtn.Dock = System.Windows.Forms.DockStyle.Left;
            this.DodajBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DodajBtn.Location = new System.Drawing.Point(3, 3);
            this.DodajBtn.Name = "DodajBtn";
            this.DodajBtn.Size = new System.Drawing.Size(91, 28);
            this.DodajBtn.TabIndex = 1;
            this.DodajBtn.Text = "Dodaj";
            this.DodajBtn.UseVisualStyleBackColor = true;
            this.DodajBtn.Click += new System.EventHandler(this.DodajBtn_Click);
            // 
            // UsunBtn
            // 
            this.UsunBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.UsunBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UsunBtn.Location = new System.Drawing.Point(326, 3);
            this.UsunBtn.Name = "UsunBtn";
            this.UsunBtn.Size = new System.Drawing.Size(91, 28);
            this.UsunBtn.TabIndex = 1;
            this.UsunBtn.Text = "Usuń";
            this.UsunBtn.UseVisualStyleBackColor = true;
            this.UsunBtn.Click += new System.EventHandler(this.UsunBtn_Click);
            // 
            // MainMenu
            // 
            this.MainMenu.Controls.Add(OrganizerTab);
            this.MainMenu.Controls.Add(this.zarzadzanieKlientamiTab);
            this.MainMenu.Controls.Add(this.zarzadzanieProduktamiTab);
            this.MainMenu.Controls.Add(this.mailingTab);
            this.MainMenu.Controls.Add(this.LogTab);
            this.MainMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MainMenu.ItemSize = new System.Drawing.Size(180, 30);
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Multiline = true;
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.SelectedIndex = 0;
            this.MainMenu.Size = new System.Drawing.Size(1147, 612);
            this.MainMenu.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.MainMenu.TabIndex = 1;
            this.MainMenu.SizeChanged += new System.EventHandler(this.MainMenu_SizeChanged);
            // 
            // zarzadzanieKlientamiTab
            // 
            this.zarzadzanieKlientamiTab.BackColor = System.Drawing.Color.WhiteSmoke;
            this.zarzadzanieKlientamiTab.Controls.Add(this.tableLayoutPanel3);
            this.zarzadzanieKlientamiTab.Location = new System.Drawing.Point(4, 34);
            this.zarzadzanieKlientamiTab.Name = "zarzadzanieKlientamiTab";
            this.zarzadzanieKlientamiTab.Padding = new System.Windows.Forms.Padding(3);
            this.zarzadzanieKlientamiTab.Size = new System.Drawing.Size(1139, 574);
            this.zarzadzanieKlientamiTab.TabIndex = 1;
            this.zarzadzanieKlientamiTab.Text = "Zarządzanie klientami";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.klienciDgv, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.049134F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.95087F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1133, 568);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.szczegolyBtn);
            this.flowLayoutPanel1.Controls.Add(this.DodajKlientaBtn);
            this.flowLayoutPanel1.Controls.Add(this.EksportBtn);
            this.flowLayoutPanel1.Controls.Add(this.WyczyscFiltryBtn);
            this.flowLayoutPanel1.Controls.Add(this.searchClientsBtn);
            this.flowLayoutPanel1.Controls.Add(this.searchClientsBox);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1127, 45);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // szczegolyBtn
            // 
            this.szczegolyBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.szczegolyBtn.Location = new System.Drawing.Point(3, 3);
            this.szczegolyBtn.Name = "szczegolyBtn";
            this.szczegolyBtn.Size = new System.Drawing.Size(150, 32);
            this.szczegolyBtn.TabIndex = 0;
            this.szczegolyBtn.Text = "Szczegóły";
            this.szczegolyBtn.UseVisualStyleBackColor = true;
            this.szczegolyBtn.Click += new System.EventHandler(this.szczegolyBtn_Click);
            // 
            // DodajKlientaBtn
            // 
            this.DodajKlientaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DodajKlientaBtn.Location = new System.Drawing.Point(159, 3);
            this.DodajKlientaBtn.Name = "DodajKlientaBtn";
            this.DodajKlientaBtn.Size = new System.Drawing.Size(150, 32);
            this.DodajKlientaBtn.TabIndex = 0;
            this.DodajKlientaBtn.Text = "Dodaj klienta";
            this.DodajKlientaBtn.UseVisualStyleBackColor = true;
            this.DodajKlientaBtn.Click += new System.EventHandler(this.DodajKlientaBtn_Click);
            // 
            // EksportBtn
            // 
            this.EksportBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EksportBtn.Location = new System.Drawing.Point(315, 3);
            this.EksportBtn.Name = "EksportBtn";
            this.EksportBtn.Size = new System.Drawing.Size(150, 32);
            this.EksportBtn.TabIndex = 0;
            this.EksportBtn.Text = "Eksport";
            this.EksportBtn.UseVisualStyleBackColor = true;
            this.EksportBtn.Click += new System.EventHandler(this.EksportBtn_Click);
            // 
            // WyczyscFiltryBtn
            // 
            this.WyczyscFiltryBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WyczyscFiltryBtn.Location = new System.Drawing.Point(471, 3);
            this.WyczyscFiltryBtn.Name = "WyczyscFiltryBtn";
            this.WyczyscFiltryBtn.Size = new System.Drawing.Size(150, 32);
            this.WyczyscFiltryBtn.TabIndex = 0;
            this.WyczyscFiltryBtn.Text = "Wyczyść Filtry";
            this.WyczyscFiltryBtn.UseVisualStyleBackColor = true;
            // 
            // searchClientsBtn
            // 
            this.searchClientsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchClientsBtn.Location = new System.Drawing.Point(627, 3);
            this.searchClientsBtn.Name = "searchClientsBtn";
            this.searchClientsBtn.Size = new System.Drawing.Size(150, 32);
            this.searchClientsBtn.TabIndex = 0;
            this.searchClientsBtn.Text = "Filtruj";
            this.searchClientsBtn.UseVisualStyleBackColor = true;
            this.searchClientsBtn.Click += new System.EventHandler(this.searchClientsBtn_Click);
            // 
            // searchClientsBox
            // 
            this.searchClientsBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchClientsBox.Location = new System.Drawing.Point(783, 3);
            this.searchClientsBox.Multiline = false;
            this.searchClientsBox.Name = "searchClientsBox";
            this.searchClientsBox.Size = new System.Drawing.Size(317, 32);
            this.searchClientsBox.TabIndex = 1;
            this.searchClientsBox.Text = "";
            // 
            // klienciDgv
            // 
            this.klienciDgv.AllowUserToAddRows = false;
            this.klienciDgv.AllowUserToDeleteRows = false;
            this.klienciDgv.AllowUserToOrderColumns = true;
            this.klienciDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.klienciDgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.klienciDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.klienciDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.klienciDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Edytuj,
            this.Usuń});
            this.klienciDgv.ContextMenuStrip = this.RightClickMenu;
            this.klienciDgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.klienciDgv.Location = new System.Drawing.Point(3, 54);
            this.klienciDgv.Name = "klienciDgv";
            this.klienciDgv.ReadOnly = true;
            this.klienciDgv.RowHeadersVisible = false;
            this.klienciDgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.klienciDgv.Size = new System.Drawing.Size(1127, 511);
            this.klienciDgv.TabIndex = 1;
            this.klienciDgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.klienciDgv_CellClick);
            this.klienciDgv.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.klienciDgv_ColumnWidthChanged);
            this.klienciDgv.Scroll += new System.Windows.Forms.ScrollEventHandler(this.klienciDgv_Scroll);
            // 
            // Edytuj
            // 
            this.Edytuj.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Edytuj.FillWeight = 184.7716F;
            this.Edytuj.HeaderText = "";
            this.Edytuj.Image = global::CRM.Properties.Resources.user_edit;
            this.Edytuj.Name = "Edytuj";
            this.Edytuj.ReadOnly = true;
            this.Edytuj.Width = 5;
            // 
            // Usuń
            // 
            this.Usuń.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Usuń.FillWeight = 15.22843F;
            this.Usuń.HeaderText = "";
            this.Usuń.Image = global::CRM.Properties.Resources.user_delete;
            this.Usuń.Name = "Usuń";
            this.Usuń.ReadOnly = true;
            this.Usuń.Width = 5;
            // 
            // RightClickMenu
            // 
            this.RightClickMenu.Name = "RightClickMenu";
            this.RightClickMenu.Size = new System.Drawing.Size(61, 4);
            this.RightClickMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.RightClickMenu_ItemClicked);
            // 
            // zarzadzanieProduktamiTab
            // 
            this.zarzadzanieProduktamiTab.BackColor = System.Drawing.Color.WhiteSmoke;
            this.zarzadzanieProduktamiTab.Controls.Add(this.tableLayoutPanel1);
            this.zarzadzanieProduktamiTab.Location = new System.Drawing.Point(4, 34);
            this.zarzadzanieProduktamiTab.Name = "zarzadzanieProduktamiTab";
            this.zarzadzanieProduktamiTab.Size = new System.Drawing.Size(1139, 574);
            this.zarzadzanieProduktamiTab.TabIndex = 2;
            this.zarzadzanieProduktamiTab.Text = "Baza Produktów";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.productsGrid, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.474748F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.52525F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1139, 574);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // productsGrid
            // 
            this.productsGrid.AllowUserToAddRows = false;
            this.productsGrid.AllowUserToDeleteRows = false;
            this.productsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.productsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.productsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.productsGrid.Location = new System.Drawing.Point(3, 45);
            this.productsGrid.Name = "productsGrid";
            this.productsGrid.ReadOnly = true;
            this.productsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.productsGrid.Size = new System.Drawing.Size(1133, 526);
            this.productsGrid.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.91829F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.50397F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.62754F));
            this.tableLayoutPanel2.Controls.Add(this.SearchProductBox, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.button2, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 8);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1133, 31);
            this.tableLayoutPanel2.TabIndex = 1;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // SearchProductBox
            // 
            this.SearchProductBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.SearchProductBox.Location = new System.Drawing.Point(3, 5);
            this.SearchProductBox.Name = "SearchProductBox";
            this.SearchProductBox.Size = new System.Drawing.Size(593, 23);
            this.SearchProductBox.TabIndex = 0;
            this.SearchProductBox.Text = "Podaj nazwę...";
            this.SearchProductBox.Click += new System.EventHandler(this.SearchProductBox_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.Location = new System.Drawing.Point(602, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(146, 25);
            this.button2.TabIndex = 1;
            this.button2.Text = "Wyszukaj Produkty";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // mailingTab
            // 
            this.mailingTab.BackColor = System.Drawing.Color.WhiteSmoke;
            this.mailingTab.Controls.Add(this.tableLayoutPanel6);
            this.mailingTab.Location = new System.Drawing.Point(4, 34);
            this.mailingTab.Name = "mailingTab";
            this.mailingTab.Size = new System.Drawing.Size(1139, 574);
            this.mailingTab.TabIndex = 3;
            this.mailingTab.Text = "Pracownicy";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel10, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.ListaKlientow, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel9, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 155F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1139, 574);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // ListaKlientow
            // 
            this.ListaKlientow.CheckOnClick = true;
            this.ListaKlientow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListaKlientow.FormattingEnabled = true;
            this.ListaKlientow.Location = new System.Drawing.Point(3, 43);
            this.ListaKlientow.Name = "ListaKlientow";
            this.ListaKlientow.ScrollAlwaysVisible = true;
            this.ListaKlientow.Size = new System.Drawing.Size(1133, 373);
            this.ListaKlientow.TabIndex = 3;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.42857F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.57143F));
            this.tableLayoutPanel9.Controls.Add(this.szukajkaKlientow, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.button1, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(1133, 34);
            this.tableLayoutPanel9.TabIndex = 4;
            // 
            // szukajkaKlientow
            // 
            this.szukajkaKlientow.Location = new System.Drawing.Point(3, 3);
            this.szukajkaKlientow.Multiline = false;
            this.szukajkaKlientow.Name = "szukajkaKlientow";
            this.szukajkaKlientow.Size = new System.Drawing.Size(803, 28);
            this.szukajkaKlientow.TabIndex = 7;
            this.szukajkaKlientow.Text = "";
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(812, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(318, 28);
            this.button1.TabIndex = 8;
            this.button1.Text = "Wyswietl Pracownikow";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // LogTab
            // 
            this.LogTab.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LogTab.Controls.Add(this.logGridView);
            this.LogTab.Location = new System.Drawing.Point(4, 34);
            this.LogTab.Name = "LogTab";
            this.LogTab.Size = new System.Drawing.Size(1139, 574);
            this.LogTab.TabIndex = 4;
            this.LogTab.Text = "Log";
            // 
            // logGridView
            // 
            this.logGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.logGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.logGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logGridView.Location = new System.Drawing.Point(0, 0);
            this.logGridView.Name = "logGridView";
            this.logGridView.Size = new System.Drawing.Size(1139, 574);
            this.logGridView.TabIndex = 0;
            // 
            // Column18
            // 
            this.Column18.Name = "Column18";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // WyczyscCol
            // 
            this.WyczyscCol.Name = "WyczyscCol";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewImageColumn1.FillWeight = 184.7716F;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::CRM.Properties.Resources.user_edit;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewImageColumn2.FillWeight = 15.22843F;
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = global::CRM.Properties.Resources.user_delete;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            // 
            // dataGridViewImageColumn3
            // 
            this.dataGridViewImageColumn3.HeaderText = "";
            this.dataGridViewImageColumn3.Image = global::CRM.Properties.Resources.user_edit;
            this.dataGridViewImageColumn3.Name = "dataGridViewImageColumn3";
            this.dataGridViewImageColumn3.Width = 30;
            // 
            // dataGridViewImageColumn4
            // 
            this.dataGridViewImageColumn4.HeaderText = "";
            this.dataGridViewImageColumn4.Image = global::CRM.Properties.Resources.user_delete;
            this.dataGridViewImageColumn4.Name = "dataGridViewImageColumn4";
            this.dataGridViewImageColumn4.Width = 30;
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.wiadomoscTxtbx, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.button3, 1, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 422);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(1133, 149);
            this.tableLayoutPanel10.TabIndex = 2;
            // 
            // wiadomoscTxtbx
            // 
            this.wiadomoscTxtbx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wiadomoscTxtbx.Location = new System.Drawing.Point(3, 3);
            this.wiadomoscTxtbx.Name = "wiadomoscTxtbx";
            this.wiadomoscTxtbx.Size = new System.Drawing.Size(560, 143);
            this.wiadomoscTxtbx.TabIndex = 0;
            this.wiadomoscTxtbx.Text = "";
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(569, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(122, 33);
            this.button3.TabIndex = 1;
            this.button3.Text = "wyslij wiadomosc";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1147, 612);
            this.Controls.Add(this.MainMenu);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MinimumSize = new System.Drawing.Size(975, 650);
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CRM";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            OrganizerTab.ResumeLayout(false);
            this.organizerTable.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.NotesGrBox.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.ListaZadanGrBox.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.MainMenu.ResumeLayout(false);
            this.zarzadzanieKlientamiTab.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.klienciDgv)).EndInit();
            this.zarzadzanieProduktamiTab.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.productsGrid)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.mailingTab.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.LogTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.logGridView)).EndInit();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MainMenu;
        private System.Windows.Forms.TabPage zarzadzanieKlientamiTab;
        private System.Windows.Forms.TabPage zarzadzanieProduktamiTab;
        private System.Windows.Forms.TabPage mailingTab;
        private System.Windows.Forms.TabPage LogTab;
        private System.Windows.Forms.Button WyczyscFiltryBtn;
        private System.Windows.Forms.Button EksportBtn;
        private System.Windows.Forms.Button DodajKlientaBtn;
        private System.Windows.Forms.Button szczegolyBtn;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn3;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewButtonColumn WyczyscCol;
        private Calendar.NET.Calendar calendar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox NotesGrBox;
        private System.Windows.Forms.TextBox NotesTxBox;
        private System.Windows.Forms.GroupBox ListaZadanGrBox;
        private System.Windows.Forms.TextBox PoleEdycjiTxBox;
        private System.Windows.Forms.Button DodajBtn;
        private System.Windows.Forms.Button UsunBtn;
        private System.Windows.Forms.ListBox ListaZadanListBox;
        private System.Windows.Forms.Button dodajSpotkanieBtn;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.ContextMenuStrip RightClickMenu;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SaveFileDialog saveFD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView productsGrid;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox SearchProductBox;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView logGridView;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.DataGridView klienciDgv;
        private System.Windows.Forms.DataGridViewImageColumn Edytuj;
        private System.Windows.Forms.DataGridViewImageColumn Usuń;
        private System.Windows.Forms.RichTextBox searchClientsBox;
        private System.Windows.Forms.Button searchClientsBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel organizerTable;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button aktualizujNotatke;
        private System.Windows.Forms.CheckedListBox ListaKlientow;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox szukajkaKlientow;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.RichTextBox wiadomoscTxtbx;
        private System.Windows.Forms.Button button3;
    }
}

