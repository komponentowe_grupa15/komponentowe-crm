﻿namespace CRM
{
    partial class SzczegolyKlienta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nazwaKlientaLbl = new System.Windows.Forms.Label();
            this.nazwaTxBox = new System.Windows.Forms.TextBox();
            this.ulicaINumerLbl = new System.Windows.Forms.Label();
            this.ulicaINumerTxBox = new System.Windows.Forms.TextBox();
            this.kodPocztowyLbl = new System.Windows.Forms.Label();
            this.kodPocztowyTxBox = new System.Windows.Forms.TextBox();
            this.miejscowoscLbl = new System.Windows.Forms.Label();
            this.miejscowoscTxBox = new System.Windows.Forms.TextBox();
            this.wojewodztwoLbl = new System.Windows.Forms.Label();
            this.krajLbl = new System.Windows.Forms.Label();
            this.nipLbl = new System.Windows.Forms.Label();
            this.nipTxBox = new System.Windows.Forms.TextBox();
            this.branzaLbl = new System.Windows.Forms.Label();
            this.statusKlientaLbl = new System.Windows.Forms.Label();
            this.adresyDgv = new System.Windows.Forms.DataGridView();
            this.RightMenuClickDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.adresyGrpBox = new System.Windows.Forms.GroupBox();
            this.KrajCBox = new System.Windows.Forms.ComboBox();
            this.wojewodztwoCBox = new System.Windows.Forms.ComboBox();
            this.rodzajAdresyCBox = new System.Windows.Forms.ComboBox();
            this.nowyAdresBtn = new System.Windows.Forms.Button();
            this.telefonLbl = new System.Windows.Forms.Label();
            this.telefonTxBox = new System.Windows.Forms.TextBox();
            this.emailTxBox = new System.Windows.Forms.TextBox();
            this.rodzajAdresuLbl = new System.Windows.Forms.Label();
            this.emailLbl = new System.Windows.Forms.Label();
            this.danePodatkoweGrpBox = new System.Windows.Forms.GroupBox();
            this.regonTxBox = new System.Windows.Forms.TextBox();
            this.regonLbl = new System.Windows.Forms.Label();
            this.stronawwwLbl = new System.Windows.Forms.Label();
            this.stronawwwTxBox = new System.Windows.Forms.TextBox();
            this.oKliencieGrpBox = new System.Windows.Forms.GroupBox();
            this.statusKlientaCBox = new System.Windows.Forms.ComboBox();
            this.branzaCBox = new System.Windows.Forms.ComboBox();
            this.uwagiTxBox = new System.Windows.Forms.TextBox();
            this.uwagiLbl = new System.Windows.Forms.Label();
            this.zapiszZmianyBtn = new System.Windows.Forms.Button();
            this.anulujBtn = new System.Windows.Forms.Button();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.EdytujCol = new System.Windows.Forms.DataGridViewImageColumn();
            this.UsunCol = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.adresyDgv)).BeginInit();
            this.adresyGrpBox.SuspendLayout();
            this.danePodatkoweGrpBox.SuspendLayout();
            this.oKliencieGrpBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // nazwaKlientaLbl
            // 
            this.nazwaKlientaLbl.AutoSize = true;
            this.nazwaKlientaLbl.Location = new System.Drawing.Point(13, 8);
            this.nazwaKlientaLbl.Name = "nazwaKlientaLbl";
            this.nazwaKlientaLbl.Size = new System.Drawing.Size(74, 13);
            this.nazwaKlientaLbl.TabIndex = 0;
            this.nazwaKlientaLbl.Text = "Nazwa klienta";
            // 
            // nazwaTxBox
            // 
            this.nazwaTxBox.Location = new System.Drawing.Point(12, 24);
            this.nazwaTxBox.Name = "nazwaTxBox";
            this.nazwaTxBox.Size = new System.Drawing.Size(707, 20);
            this.nazwaTxBox.TabIndex = 1;
            // 
            // ulicaINumerLbl
            // 
            this.ulicaINumerLbl.AutoSize = true;
            this.ulicaINumerLbl.Location = new System.Drawing.Point(7, 16);
            this.ulicaINumerLbl.Name = "ulicaINumerLbl";
            this.ulicaINumerLbl.Size = new System.Drawing.Size(68, 13);
            this.ulicaINumerLbl.TabIndex = 0;
            this.ulicaINumerLbl.Text = "Ulica i numer";
            // 
            // ulicaINumerTxBox
            // 
            this.ulicaINumerTxBox.Location = new System.Drawing.Point(6, 32);
            this.ulicaINumerTxBox.Name = "ulicaINumerTxBox";
            this.ulicaINumerTxBox.Size = new System.Drawing.Size(355, 20);
            this.ulicaINumerTxBox.TabIndex = 1;
            // 
            // kodPocztowyLbl
            // 
            this.kodPocztowyLbl.AutoSize = true;
            this.kodPocztowyLbl.Location = new System.Drawing.Point(365, 16);
            this.kodPocztowyLbl.Name = "kodPocztowyLbl";
            this.kodPocztowyLbl.Size = new System.Drawing.Size(74, 13);
            this.kodPocztowyLbl.TabIndex = 0;
            this.kodPocztowyLbl.Text = "Kod pocztowy";
            // 
            // kodPocztowyTxBox
            // 
            this.kodPocztowyTxBox.Location = new System.Drawing.Point(364, 32);
            this.kodPocztowyTxBox.Name = "kodPocztowyTxBox";
            this.kodPocztowyTxBox.Size = new System.Drawing.Size(100, 20);
            this.kodPocztowyTxBox.TabIndex = 1;
            // 
            // miejscowoscLbl
            // 
            this.miejscowoscLbl.AutoSize = true;
            this.miejscowoscLbl.Location = new System.Drawing.Point(470, 16);
            this.miejscowoscLbl.Name = "miejscowoscLbl";
            this.miejscowoscLbl.Size = new System.Drawing.Size(68, 13);
            this.miejscowoscLbl.TabIndex = 0;
            this.miejscowoscLbl.Text = "Miejscowość";
            // 
            // miejscowoscTxBox
            // 
            this.miejscowoscTxBox.Location = new System.Drawing.Point(469, 32);
            this.miejscowoscTxBox.Name = "miejscowoscTxBox";
            this.miejscowoscTxBox.Size = new System.Drawing.Size(232, 20);
            this.miejscowoscTxBox.TabIndex = 1;
            // 
            // wojewodztwoLbl
            // 
            this.wojewodztwoLbl.AutoSize = true;
            this.wojewodztwoLbl.Location = new System.Drawing.Point(6, 55);
            this.wojewodztwoLbl.Name = "wojewodztwoLbl";
            this.wojewodztwoLbl.Size = new System.Drawing.Size(74, 13);
            this.wojewodztwoLbl.TabIndex = 0;
            this.wojewodztwoLbl.Text = "Województwo";
            // 
            // krajLbl
            // 
            this.krajLbl.AutoSize = true;
            this.krajLbl.Location = new System.Drawing.Point(191, 55);
            this.krajLbl.Name = "krajLbl";
            this.krajLbl.Size = new System.Drawing.Size(25, 13);
            this.krajLbl.TabIndex = 0;
            this.krajLbl.Text = "Kraj";
            // 
            // nipLbl
            // 
            this.nipLbl.AutoSize = true;
            this.nipLbl.Location = new System.Drawing.Point(7, 16);
            this.nipLbl.Name = "nipLbl";
            this.nipLbl.Size = new System.Drawing.Size(25, 13);
            this.nipLbl.TabIndex = 0;
            this.nipLbl.Text = "NIP";
            // 
            // nipTxBox
            // 
            this.nipTxBox.Location = new System.Drawing.Point(6, 32);
            this.nipTxBox.Name = "nipTxBox";
            this.nipTxBox.Size = new System.Drawing.Size(155, 20);
            this.nipTxBox.TabIndex = 1;
            // 
            // branzaLbl
            // 
            this.branzaLbl.AutoSize = true;
            this.branzaLbl.Location = new System.Drawing.Point(6, 59);
            this.branzaLbl.Name = "branzaLbl";
            this.branzaLbl.Size = new System.Drawing.Size(40, 13);
            this.branzaLbl.TabIndex = 0;
            this.branzaLbl.Text = "Branża";
            // 
            // statusKlientaLbl
            // 
            this.statusKlientaLbl.AutoSize = true;
            this.statusKlientaLbl.Location = new System.Drawing.Point(6, 100);
            this.statusKlientaLbl.Name = "statusKlientaLbl";
            this.statusKlientaLbl.Size = new System.Drawing.Size(71, 13);
            this.statusKlientaLbl.TabIndex = 0;
            this.statusKlientaLbl.Text = "Status klienta";
            // 
            // adresyDgv
            // 
            this.adresyDgv.AllowUserToAddRows = false;
            this.adresyDgv.AllowUserToDeleteRows = false;
            this.adresyDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.adresyDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EdytujCol,
            this.UsunCol});
            this.adresyDgv.ContextMenuStrip = this.RightMenuClickDetail;
            this.adresyDgv.Location = new System.Drawing.Point(4, 97);
            this.adresyDgv.MultiSelect = false;
            this.adresyDgv.Name = "adresyDgv";
            this.adresyDgv.ReadOnly = true;
            this.adresyDgv.RowHeadersVisible = false;
            this.adresyDgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.adresyDgv.Size = new System.Drawing.Size(525, 152);
            this.adresyDgv.TabIndex = 2;
            this.adresyDgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.adresyDgv_CellClick);
            // 
            // RightMenuClickDetail
            // 
            this.RightMenuClickDetail.Name = "RightMenuClickDetail";
            this.RightMenuClickDetail.Size = new System.Drawing.Size(61, 4);
            // 
            // adresyGrpBox
            // 
            this.adresyGrpBox.Controls.Add(this.KrajCBox);
            this.adresyGrpBox.Controls.Add(this.wojewodztwoCBox);
            this.adresyGrpBox.Controls.Add(this.rodzajAdresyCBox);
            this.adresyGrpBox.Controls.Add(this.nowyAdresBtn);
            this.adresyGrpBox.Controls.Add(this.ulicaINumerLbl);
            this.adresyGrpBox.Controls.Add(this.adresyDgv);
            this.adresyGrpBox.Controls.Add(this.ulicaINumerTxBox);
            this.adresyGrpBox.Controls.Add(this.kodPocztowyLbl);
            this.adresyGrpBox.Controls.Add(this.kodPocztowyTxBox);
            this.adresyGrpBox.Controls.Add(this.miejscowoscLbl);
            this.adresyGrpBox.Controls.Add(this.miejscowoscTxBox);
            this.adresyGrpBox.Controls.Add(this.telefonLbl);
            this.adresyGrpBox.Controls.Add(this.wojewodztwoLbl);
            this.adresyGrpBox.Controls.Add(this.telefonTxBox);
            this.adresyGrpBox.Controls.Add(this.emailTxBox);
            this.adresyGrpBox.Controls.Add(this.rodzajAdresuLbl);
            this.adresyGrpBox.Controls.Add(this.emailLbl);
            this.adresyGrpBox.Controls.Add(this.krajLbl);
            this.adresyGrpBox.Location = new System.Drawing.Point(12, 50);
            this.adresyGrpBox.Name = "adresyGrpBox";
            this.adresyGrpBox.Size = new System.Drawing.Size(707, 255);
            this.adresyGrpBox.TabIndex = 3;
            this.adresyGrpBox.TabStop = false;
            this.adresyGrpBox.Text = "Adresy";
            // 
            // KrajCBox
            // 
            this.KrajCBox.FormattingEnabled = true;
            this.KrajCBox.Location = new System.Drawing.Point(190, 70);
            this.KrajCBox.MaxDropDownItems = 50;
            this.KrajCBox.Name = "KrajCBox";
            this.KrajCBox.Size = new System.Drawing.Size(170, 21);
            this.KrajCBox.TabIndex = 4;
            // 
            // wojewodztwoCBox
            // 
            this.wojewodztwoCBox.FormattingEnabled = true;
            this.wojewodztwoCBox.Location = new System.Drawing.Point(6, 70);
            this.wojewodztwoCBox.MaxDropDownItems = 50;
            this.wojewodztwoCBox.Name = "wojewodztwoCBox";
            this.wojewodztwoCBox.Size = new System.Drawing.Size(178, 21);
            this.wojewodztwoCBox.TabIndex = 4;
            // 
            // rodzajAdresyCBox
            // 
            this.rodzajAdresyCBox.FormattingEnabled = true;
            this.rodzajAdresyCBox.IntegralHeight = false;
            this.rodzajAdresyCBox.ItemHeight = 13;
            this.rodzajAdresyCBox.Items.AddRange(new object[] {
            "Ogólny",
            "Korespondencyjny",
            "Do faktury",
            "Domowy"});
            this.rodzajAdresyCBox.Location = new System.Drawing.Point(535, 114);
            this.rodzajAdresyCBox.MaxDropDownItems = 50;
            this.rodzajAdresyCBox.Name = "rodzajAdresyCBox";
            this.rodzajAdresyCBox.Size = new System.Drawing.Size(166, 21);
            this.rodzajAdresyCBox.TabIndex = 4;
            // 
            // nowyAdresBtn
            // 
            this.nowyAdresBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nowyAdresBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nowyAdresBtn.Location = new System.Drawing.Point(562, 173);
            this.nowyAdresBtn.Name = "nowyAdresBtn";
            this.nowyAdresBtn.Size = new System.Drawing.Size(114, 35);
            this.nowyAdresBtn.TabIndex = 3;
            this.nowyAdresBtn.Text = "Nowy adres";
            this.nowyAdresBtn.UseVisualStyleBackColor = true;
            this.nowyAdresBtn.Click += new System.EventHandler(this.nowyAdresBtn_Click);
            // 
            // telefonLbl
            // 
            this.telefonLbl.AutoSize = true;
            this.telefonLbl.Location = new System.Drawing.Point(367, 55);
            this.telefonLbl.Name = "telefonLbl";
            this.telefonLbl.Size = new System.Drawing.Size(43, 13);
            this.telefonLbl.TabIndex = 0;
            this.telefonLbl.Text = "Telefon";
            // 
            // telefonTxBox
            // 
            this.telefonTxBox.Location = new System.Drawing.Point(366, 71);
            this.telefonTxBox.Name = "telefonTxBox";
            this.telefonTxBox.Size = new System.Drawing.Size(163, 20);
            this.telefonTxBox.TabIndex = 1;
            // 
            // emailTxBox
            // 
            this.emailTxBox.Location = new System.Drawing.Point(538, 71);
            this.emailTxBox.Name = "emailTxBox";
            this.emailTxBox.Size = new System.Drawing.Size(163, 20);
            this.emailTxBox.TabIndex = 1;
            // 
            // rodzajAdresuLbl
            // 
            this.rodzajAdresuLbl.AutoSize = true;
            this.rodzajAdresuLbl.Location = new System.Drawing.Point(539, 98);
            this.rodzajAdresuLbl.Name = "rodzajAdresuLbl";
            this.rodzajAdresuLbl.Size = new System.Drawing.Size(75, 13);
            this.rodzajAdresuLbl.TabIndex = 0;
            this.rodzajAdresuLbl.Text = "Rodzaj adresu";
            // 
            // emailLbl
            // 
            this.emailLbl.AutoSize = true;
            this.emailLbl.Location = new System.Drawing.Point(539, 55);
            this.emailLbl.Name = "emailLbl";
            this.emailLbl.Size = new System.Drawing.Size(64, 13);
            this.emailLbl.TabIndex = 0;
            this.emailLbl.Text = "Adres e-mail";
            // 
            // danePodatkoweGrpBox
            // 
            this.danePodatkoweGrpBox.Controls.Add(this.regonTxBox);
            this.danePodatkoweGrpBox.Controls.Add(this.regonLbl);
            this.danePodatkoweGrpBox.Controls.Add(this.nipTxBox);
            this.danePodatkoweGrpBox.Controls.Add(this.nipLbl);
            this.danePodatkoweGrpBox.Location = new System.Drawing.Point(12, 311);
            this.danePodatkoweGrpBox.Name = "danePodatkoweGrpBox";
            this.danePodatkoweGrpBox.Size = new System.Drawing.Size(168, 113);
            this.danePodatkoweGrpBox.TabIndex = 4;
            this.danePodatkoweGrpBox.TabStop = false;
            this.danePodatkoweGrpBox.Text = "Dane podatkowe";
            // 
            // regonTxBox
            // 
            this.regonTxBox.Location = new System.Drawing.Point(6, 75);
            this.regonTxBox.Name = "regonTxBox";
            this.regonTxBox.Size = new System.Drawing.Size(155, 20);
            this.regonTxBox.TabIndex = 1;
            // 
            // regonLbl
            // 
            this.regonLbl.AutoSize = true;
            this.regonLbl.Location = new System.Drawing.Point(7, 59);
            this.regonLbl.Name = "regonLbl";
            this.regonLbl.Size = new System.Drawing.Size(46, 13);
            this.regonLbl.TabIndex = 0;
            this.regonLbl.Text = "REGON";
            // 
            // stronawwwLbl
            // 
            this.stronawwwLbl.AutoSize = true;
            this.stronawwwLbl.Location = new System.Drawing.Point(6, 16);
            this.stronawwwLbl.Name = "stronawwwLbl";
            this.stronawwwLbl.Size = new System.Drawing.Size(65, 13);
            this.stronawwwLbl.TabIndex = 0;
            this.stronawwwLbl.Text = "Strona www";
            // 
            // stronawwwTxBox
            // 
            this.stronawwwTxBox.Location = new System.Drawing.Point(5, 32);
            this.stronawwwTxBox.Name = "stronawwwTxBox";
            this.stronawwwTxBox.Size = new System.Drawing.Size(181, 20);
            this.stronawwwTxBox.TabIndex = 1;
            // 
            // oKliencieGrpBox
            // 
            this.oKliencieGrpBox.Controls.Add(this.statusKlientaCBox);
            this.oKliencieGrpBox.Controls.Add(this.branzaCBox);
            this.oKliencieGrpBox.Controls.Add(this.uwagiTxBox);
            this.oKliencieGrpBox.Controls.Add(this.stronawwwTxBox);
            this.oKliencieGrpBox.Controls.Add(this.uwagiLbl);
            this.oKliencieGrpBox.Controls.Add(this.stronawwwLbl);
            this.oKliencieGrpBox.Controls.Add(this.branzaLbl);
            this.oKliencieGrpBox.Controls.Add(this.statusKlientaLbl);
            this.oKliencieGrpBox.Location = new System.Drawing.Point(186, 311);
            this.oKliencieGrpBox.Name = "oKliencieGrpBox";
            this.oKliencieGrpBox.Size = new System.Drawing.Size(533, 140);
            this.oKliencieGrpBox.TabIndex = 5;
            this.oKliencieGrpBox.TabStop = false;
            this.oKliencieGrpBox.Text = "O kliencie";
            // 
            // statusKlientaCBox
            // 
            this.statusKlientaCBox.FormattingEnabled = true;
            this.statusKlientaCBox.Location = new System.Drawing.Point(5, 113);
            this.statusKlientaCBox.MaxDropDownItems = 50;
            this.statusKlientaCBox.Name = "statusKlientaCBox";
            this.statusKlientaCBox.Size = new System.Drawing.Size(182, 21);
            this.statusKlientaCBox.TabIndex = 4;
            // 
            // branzaCBox
            // 
            this.branzaCBox.FormattingEnabled = true;
            this.branzaCBox.Location = new System.Drawing.Point(5, 75);
            this.branzaCBox.MaxDropDownItems = 50;
            this.branzaCBox.Name = "branzaCBox";
            this.branzaCBox.Size = new System.Drawing.Size(182, 21);
            this.branzaCBox.TabIndex = 4;
            // 
            // uwagiTxBox
            // 
            this.uwagiTxBox.Location = new System.Drawing.Point(190, 31);
            this.uwagiTxBox.Multiline = true;
            this.uwagiTxBox.Name = "uwagiTxBox";
            this.uwagiTxBox.Size = new System.Drawing.Size(334, 103);
            this.uwagiTxBox.TabIndex = 1;
            // 
            // uwagiLbl
            // 
            this.uwagiLbl.AutoSize = true;
            this.uwagiLbl.Location = new System.Drawing.Point(191, 15);
            this.uwagiLbl.Name = "uwagiLbl";
            this.uwagiLbl.Size = new System.Drawing.Size(37, 13);
            this.uwagiLbl.TabIndex = 0;
            this.uwagiLbl.Text = "Uwagi";
            // 
            // zapiszZmianyBtn
            // 
            this.zapiszZmianyBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.zapiszZmianyBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapiszZmianyBtn.Location = new System.Drawing.Point(605, 457);
            this.zapiszZmianyBtn.Name = "zapiszZmianyBtn";
            this.zapiszZmianyBtn.Size = new System.Drawing.Size(114, 35);
            this.zapiszZmianyBtn.TabIndex = 3;
            this.zapiszZmianyBtn.Text = "Zapisz zmiany";
            this.zapiszZmianyBtn.UseVisualStyleBackColor = true;
            this.zapiszZmianyBtn.Click += new System.EventHandler(this.zapiszZmianyBtn_Click);
            // 
            // anulujBtn
            // 
            this.anulujBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.anulujBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anulujBtn.Location = new System.Drawing.Point(481, 457);
            this.anulujBtn.Name = "anulujBtn";
            this.anulujBtn.Size = new System.Drawing.Size(114, 35);
            this.anulujBtn.TabIndex = 3;
            this.anulujBtn.Text = "Anuluj";
            this.anulujBtn.UseVisualStyleBackColor = true;
            this.anulujBtn.Click += new System.EventHandler(this.anulujBtn_Click);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::CRM.Properties.Resources.file_edit;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 30;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = global::CRM.Properties.Resources.file_delete;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Width = 30;
            // 
            // EdytujCol
            // 
            this.EdytujCol.HeaderText = "";
            this.EdytujCol.Image = global::CRM.Properties.Resources.file_edit;
            this.EdytujCol.Name = "EdytujCol";
            this.EdytujCol.ReadOnly = true;
            this.EdytujCol.Width = 30;
            // 
            // UsunCol
            // 
            this.UsunCol.HeaderText = "";
            this.UsunCol.Image = global::CRM.Properties.Resources.file_delete;
            this.UsunCol.Name = "UsunCol";
            this.UsunCol.ReadOnly = true;
            this.UsunCol.Width = 30;
            // 
            // SzczegolyKlienta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 499);
            this.Controls.Add(this.oKliencieGrpBox);
            this.Controls.Add(this.danePodatkoweGrpBox);
            this.Controls.Add(this.anulujBtn);
            this.Controls.Add(this.zapiszZmianyBtn);
            this.Controls.Add(this.adresyGrpBox);
            this.Controls.Add(this.nazwaTxBox);
            this.Controls.Add(this.nazwaKlientaLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SzczegolyKlienta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Szczegółowe dane klienta";
            this.Load += new System.EventHandler(this.SzczegolyKlienta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.adresyDgv)).EndInit();
            this.adresyGrpBox.ResumeLayout(false);
            this.adresyGrpBox.PerformLayout();
            this.danePodatkoweGrpBox.ResumeLayout(false);
            this.danePodatkoweGrpBox.PerformLayout();
            this.oKliencieGrpBox.ResumeLayout(false);
            this.oKliencieGrpBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nazwaKlientaLbl;
        private System.Windows.Forms.TextBox nazwaTxBox;
        private System.Windows.Forms.Label ulicaINumerLbl;
        private System.Windows.Forms.TextBox ulicaINumerTxBox;
        private System.Windows.Forms.Label kodPocztowyLbl;
        private System.Windows.Forms.TextBox kodPocztowyTxBox;
        private System.Windows.Forms.Label miejscowoscLbl;
        private System.Windows.Forms.TextBox miejscowoscTxBox;
        private System.Windows.Forms.Label wojewodztwoLbl;
        private System.Windows.Forms.Label krajLbl;
        private System.Windows.Forms.Label nipLbl;
        private System.Windows.Forms.TextBox nipTxBox;
        private System.Windows.Forms.Label branzaLbl;
        private System.Windows.Forms.Label statusKlientaLbl;
        private System.Windows.Forms.DataGridView adresyDgv;
        private System.Windows.Forms.GroupBox adresyGrpBox;
        private System.Windows.Forms.ComboBox wojewodztwoCBox;
        private System.Windows.Forms.ComboBox rodzajAdresyCBox;
        private System.Windows.Forms.Label telefonLbl;
        private System.Windows.Forms.TextBox telefonTxBox;
        private System.Windows.Forms.TextBox emailTxBox;
        private System.Windows.Forms.Label rodzajAdresuLbl;
        private System.Windows.Forms.Label emailLbl;
        private System.Windows.Forms.GroupBox danePodatkoweGrpBox;
        private System.Windows.Forms.TextBox regonTxBox;
        private System.Windows.Forms.Label regonLbl;
        private System.Windows.Forms.Label stronawwwLbl;
        private System.Windows.Forms.TextBox stronawwwTxBox;
        private System.Windows.Forms.GroupBox oKliencieGrpBox;
        private System.Windows.Forms.ComboBox statusKlientaCBox;
        private System.Windows.Forms.ComboBox branzaCBox;
        private System.Windows.Forms.TextBox uwagiTxBox;
        private System.Windows.Forms.Label uwagiLbl;
        private System.Windows.Forms.Button zapiszZmianyBtn;
        private System.Windows.Forms.Button anulujBtn;
        private System.Windows.Forms.Button nowyAdresBtn;
        private System.Windows.Forms.ComboBox KrajCBox;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.ContextMenuStrip RightMenuClickDetail;
        private System.Windows.Forms.DataGridViewImageColumn EdytujCol;
        private System.Windows.Forms.DataGridViewImageColumn UsunCol;
    }
}