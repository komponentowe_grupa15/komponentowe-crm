﻿namespace CRM.forms
{
    partial class DodajDoKalendarzaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.opisTxtBox = new System.Windows.Forms.TextBox();
            this.dodajBtn = new Calendar.NET.CoolButton();
            this.anulujBtn = new Calendar.NET.CoolButton();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(183, 34);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(102, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(22, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Opis wydarzenia";
            // 
            // opisTxtBox
            // 
            this.opisTxtBox.Location = new System.Drawing.Point(183, 78);
            this.opisTxtBox.Multiline = true;
            this.opisTxtBox.Name = "opisTxtBox";
            this.opisTxtBox.Size = new System.Drawing.Size(200, 93);
            this.opisTxtBox.TabIndex = 2;
            // 
            // dodajBtn
            // 
            this.dodajBtn.BackColor = System.Drawing.Color.Transparent;
            this.dodajBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.dodajBtn.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.dodajBtn.ButtonFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.dodajBtn.ButtonText = "Dodaj";
            this.dodajBtn.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(144)))), ((int)(((byte)(254)))));
            this.dodajBtn.HighlightBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(198)))), ((int)(((byte)(198)))));
            this.dodajBtn.HighlightButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(198)))), ((int)(((byte)(198)))));
            this.dodajBtn.Location = new System.Drawing.Point(26, 207);
            this.dodajBtn.Name = "dodajBtn";
            this.dodajBtn.Size = new System.Drawing.Size(169, 29);
            this.dodajBtn.TabIndex = 4;
            this.dodajBtn.TextColor = System.Drawing.Color.Black;
            this.dodajBtn.Load += new System.EventHandler(this.dodajBtn_Load);
            this.dodajBtn.Click += new System.EventHandler(this.dodajDoKalendarzaBtn_Click);
            // 
            // anulujBtn
            // 
            this.anulujBtn.BackColor = System.Drawing.Color.Transparent;
            this.anulujBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.anulujBtn.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.anulujBtn.ButtonFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.anulujBtn.ButtonText = "Anuluj";
            this.anulujBtn.FocusColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(144)))), ((int)(((byte)(254)))));
            this.anulujBtn.HighlightBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(198)))), ((int)(((byte)(198)))));
            this.anulujBtn.HighlightButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(198)))), ((int)(((byte)(198)))));
            this.anulujBtn.Location = new System.Drawing.Point(214, 207);
            this.anulujBtn.Name = "anulujBtn";
            this.anulujBtn.Size = new System.Drawing.Size(169, 29);
            this.anulujBtn.TabIndex = 4;
            this.anulujBtn.TextColor = System.Drawing.Color.Black;
            this.anulujBtn.Click += new System.EventHandler(this.AnulujBtn_Click);
            // 
            // DodajDoKalendarzaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 261);
            this.Controls.Add(this.anulujBtn);
            this.Controls.Add(this.dodajBtn);
            this.Controls.Add(this.opisTxtBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DodajDoKalendarzaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DodajDoKalendarzaForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox opisTxtBox;
        private Calendar.NET.CoolButton dodajBtn;
        private Calendar.NET.CoolButton anulujBtn;
    }
}