﻿using ClientServiceWrapper;
using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CustomService;
using Calendar.NET;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using CRM.forms;
using System.Threading;


namespace CRM
{
    public partial class SzczegolyKlienta : Form
    {
        ClientServiceWrapper.MainManager _services = ClientServiceWrapper.MainManager.Instance;
        List<Adresy> listaAdresowa = new List<Adresy>();
        int klientID = -1;
        MainForm _form;

        public SzczegolyKlienta()
        {
            InitializeComponent();
        }



        public SzczegolyKlienta(Klienci klient, MainForm form)
        {
            InitializeComponent();
            this._form = form;
            ThreadPool.QueueUserWorkItem(new WaitCallback(a => this.GetComboBoxValues()));


            if (klient != null)
            {
                var getAddress = _services.Klienci.BeginWcfCall(x => x.ZnajdzAdresy(klient.KlientId));
                this.nazwaTxBox.Text = klient.Nazwa_Klienta;
                this.nipTxBox.Text = klient.NIP;
                this.regonTxBox.Text = klient.REGON;
                this.stronawwwTxBox.Text = klient.Strona_WWW;
                this.branzaCBox.Text = klient.Branza_Id.ToString();
                this.statusKlientaCBox.Text = klient.Status_Klienta_Id.ToString();
                this.uwagiTxBox.Text = klient.Uwagi;
                this.klientID = klient.KlientId;

                listaAdresowa.AddRange(getAddress.End());
                //ThreadPool.QueueUserWorkItem(new WaitCallback(x => UpdateAdresyDgv()));
                UpdateAdresyDgv();
            }
        }

        private void zapiszZmianyBtn_Click(object sender, EventArgs e)
        {
            if (adresyDgv.Rows.Count != 0)
            {
                Klienci klient = new Klienci();
                klient.Nazwa_Klienta = nazwaTxBox.Text;
                klient.NIP = nipTxBox.Text;
                klient.REGON = regonTxBox.Text;
                klient.Strona_WWW = stronawwwTxBox.Text;
                klient.Status_Klienta_Id = statusKlientaCBox.SelectedIndex + 1;
                klient.Branza_Id = branzaCBox.SelectedIndex + 1;
                klient.UzytkownikId = _services.UserInfo.UzytkownikId;
                klient.Uwagi = uwagiTxBox.Text;

                if (klientID == -1) //dodawanie nowego klienta
                {
                    klientID = MainManager.Instance.Klienci.DodajKlienta(klient);
                    if (klientID != -1)
                    {
                        List<int> listaId = new List<int>();
                        foreach (var item in listaAdresowa)
                        {
                            if (item.AdresId == 0) listaId.Add(MainManager.Instance.Klienci.DodajAdres(item));
                            else MainManager.Instance.Klienci.EdytujAdres(item.AdresId, item);
                        }
                        MainManager.Instance.Klienci.PrzypiszAdresy(klientID, listaId);

                        MessageBox.Show("Klient został poprawnie dodany!", "Sukces!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        _form.UpdateDateGridViewForClients();
                        this.Close();
                    }
                }
                else //esycja istniejącego
                {
                    MainManager.Instance.Klienci.EdytujKlienta(klientID, klient);

                    List<int> listaId = new List<int>();
                    foreach (var item in listaAdresowa)
                    {
                        if (item.AdresId == 0) listaId.Add(MainManager.Instance.Klienci.DodajAdres(item));
                        else MainManager.Instance.Klienci.EdytujAdres(item.AdresId, item);
                    }
                    MainManager.Instance.Klienci.PrzypiszAdresy(klientID, listaId);

                    MessageBox.Show("Zaktualizowano dane klienta!", "Sukces!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Klient musi posiadać conajmniej jeden adres");
            }
            ThreadPool.QueueUserWorkItem(new WaitCallback(x => _form.UpdateDateGridViewForClients()));
        }

        private void anulujBtn_Click(object sender, EventArgs e)
        {
            this.Close();
            _form.szczegolyKlienta = new SzczegolyKlienta();
        }

        private void SzczegolyKlienta_Load(object sender, EventArgs e)
        {

        }

        private void nowyAdresBtn_Click(object sender, EventArgs e)
        {
            Adresy adres = new Adresy();

            adres.Ulica_i_Numer = ulicaINumerTxBox.Text;
            adres.Email = emailTxBox.Text;
            adres.Telefon = telefonTxBox.Text;
            adres.Kod_Pocztowy = kodPocztowyTxBox.Text;
            adres.Miejscowość = miejscowoscTxBox.Text;
            adres.PanstwoId = KrajCBox.SelectedIndex + 1;
            adres.RodzajAdresuID = rodzajAdresyCBox.SelectedIndex + 1;
            adres.WojewodztwoId = wojewodztwoCBox.SelectedIndex + 1;

            listaAdresowa.Add(adres);
            UpdateAdresyDgv();
           // ThreadPool.QueueUserWorkItem(new WaitCallback(x => UpdateAdresyDgv()));
        }


        public void GetComboBoxValues()
        {
            if (MainManager.Instance.CacheManager.GetValue<List<string>>("branze") == null)
            {
                MainManager.Instance.CacheManager.Add<List<string>>("branze", MainManager.Instance.Klienci.PobierzBranze().Select(x => x.Branza).ToList<string>());
                MainManager.Instance.CacheManager.Add<List<string>>("statusKlienta", MainManager.Instance.Klienci.PobierzStatusy().Select(x => x.Status).ToList<string>());
                MainManager.Instance.CacheManager.Add<List<string>>("wojewodztwa", MainManager.Instance.Klienci.PobierzWojewodztwa().Select(x => x.Wojewodztwo).ToList<string>());
                MainManager.Instance.CacheManager.Add<List<string>>("kraje", MainManager.Instance.Klienci.PobierzPanstwa().Select(x => x.Panstwo).ToList<string>());
                MainManager.Instance.CacheManager.Add<List<string>>("rodzajeAdresow", MainManager.Instance.Klienci.PobierzRodzaje().Select(x => x.Rodzaj).ToList<string>());
            }

            GetValues(branzaCBox, MainManager.Instance.CacheManager.GetValue<List<string>>("branze"));
            GetValues(statusKlientaCBox, MainManager.Instance.CacheManager.GetValue<List<string>>("statusKlienta"));
            GetValues(wojewodztwoCBox, MainManager.Instance.CacheManager.GetValue<List<string>>("wojewodztwa"));
            GetValues(KrajCBox, MainManager.Instance.CacheManager.GetValue<List<string>>("kraje"));
            GetValues(rodzajAdresyCBox, MainManager.Instance.CacheManager.GetValue<List<string>>("rodzajeAdresow"));
        }

        private static void GetValues(ComboBox cb, List<string> list)
        {

            if (cb.InvokeRequired)
            {
                cb.Invoke(new Action(()
                    => cb.DataSource = list));
            }
            else
            {
                cb.DataSource = list;
            }

        }

        private void adresyDgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0) //edycja
            {
                if (int.Parse(adresyDgv[2, e.RowIndex].Value.ToString())!=0)
                {
                    Adresy adres = ClientServiceWrapper.MainManager.Instance.Klienci.ZnajdzAdresy(klientID).Where(x => x.AdresId == int.Parse(adresyDgv["AdresId", e.RowIndex].Value.ToString())).First();
                ulicaINumerTxBox.Text = adres.Ulica_i_Numer;
                kodPocztowyTxBox.Text = adres.Kod_Pocztowy;
                miejscowoscTxBox.Text = adres.Miejscowość;
                wojewodztwoCBox.SelectedIndex = adres.WojewodztwoId - 1;
                KrajCBox.SelectedIndex = adres.PanstwoId - 1;
                telefonTxBox.Text = adres.Telefon;
                emailTxBox.Text = adres.Email;
                rodzajAdresyCBox.SelectedIndex = adres.RodzajAdresuID - 1;
                }

                

            }
            else if (e.ColumnIndex == 1) //usuwanie
            {
                if (MessageBox.Show("Czy jesteś tego pewien?", "Ostrzeżenie", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (int.Parse(adresyDgv[2, e.RowIndex].Value.ToString()) != 0)
                    {
                        Adresy adres = ClientServiceWrapper.MainManager.Instance.Klienci.ZnajdzAdresy(klientID).Where(x => x.AdresId == int.Parse(adresyDgv[2, e.RowIndex].Value.ToString())).First();

                        ClientServiceWrapper.MainManager.Instance.Klienci.UsunKlienta(adres.AdresId);
                        //usunąć przypisanie
                        ClientServiceWrapper.MainManager.Instance.Klienci.UsunPrzypisaniePoIdAdresu(adres.AdresId);
                        listaAdresowa.RemoveAt(e.RowIndex);
                    }
                    else listaAdresowa.RemoveAt(e.RowIndex);

                    UpdateAdresyDgv();
                }
            }

        }

        public void UpdateAdresyDgv()
        {
            
            var source = new BindingSource();
            source.DataSource = this.listaAdresowa
                 .Select(x => new
                {
                    x.AdresId,
                    x.Ulica_i_Numer,
                    x.Kod_Pocztowy,
                    x.Miejscowość,
                    Wojewodztwa = new Wojewodztwa()
                    {
                        Id = (x.Wojewodztwa != null ? x.Wojewodztwa.Id : wojewodztwoCBox.SelectedIndex),//wojCboxSelectedIndex),
                        Wojewodztwo = (x.Wojewodztwa != null ? x.Wojewodztwa.Wojewodztwo : wojewodztwoCBox.SelectedItem.ToString())//wojCboxSelectedItem)
                    }.Wojewodztwo,
                    Panstwa = new Panstwa()
                    {
                        PanstwoID = (x.Panstwa != null ? x.Panstwa.PanstwoID : rodzajAdresyCBox.SelectedIndex),//panstwoCboxSelectedIndex),
                        Panstwo = (x.Panstwa != null ? x.Panstwa.Panstwo : rodzajAdresyCBox.SelectedItem.ToString())//panstwoCboxSelectedItem)
                    }.Panstwo,

                    x.Telefon,
                    x.Email,
                    RodzajAdresu = new RodzajAdresu()
                    {
                        RodzajAdresuID = (x.RodzajAdresu != null ? x.RodzajAdresu.RodzajAdresuID : rodzajAdresyCBox.SelectedIndex),//rodzajCboxSelectedIndex),
                        Rodzaj = (x.RodzajAdresu != null ? x.RodzajAdresu.Rodzaj : rodzajAdresyCBox.SelectedItem.ToString())//rodzajCboxSelectedItem)
                    }.Rodzaj,
                });

            if (adresyDgv.InvokeRequired)
            {
                adresyDgv.Invoke(new Action(()
                    => {
                        adresyDgv.DataSource = source;
                        adresyDgv.Refresh();
                    }));
            }
            else
            {
                adresyDgv.DataSource = source;
                adresyDgv.Refresh();
            }

           
            
        }
    }
}
