﻿using Castle.Facilities.WcfIntegration;
using ClientServiceWrapper;
using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRM.forms
{
    public partial class DodajDoKalendarzaForm : Form
    {
        private Calendar.NET.Calendar _calendar;
        public DodajDoKalendarzaForm(Calendar.NET.Calendar calendar)
        {
            InitializeComponent();
            _calendar = calendar;
        }

        private void AnulujBtn_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void dodajDoKalendarzaBtn_Click(object sender, EventArgs e)
        {
            var add = ClientServiceWrapper.MainManager.Instance.Organizer.BeginWcfCall(x => x.DodajSpotkanie(new Spotkanies()
                 {
                     Data = dateTimePicker1.Value,
                     UzytkownikId = MainManager.Instance.UserInfo.UzytkownikId,
                     Nazwa = opisTxtBox.Text
                 }));

            _calendar.AddEvent(new Calendar.NET.CustomEvent()
            {
                Date = dateTimePicker1.Value,
                EventText = opisTxtBox.Text,
                EventLengthInHours = 1f
            });
            ClientServiceWrapper.MainManager.Instance.AddLog(new Historia() { Akcja = "Dodano spotkanie do kalendarza", Data = DateTime.Now, UzytkownikID = ClientServiceWrapper.MainManager.Instance.UserInfo.UzytkownikId });
            add.End();
            this.Close();
        }

        private void dodajBtn_Load(object sender, EventArgs e)
        {

        }
    }
}
