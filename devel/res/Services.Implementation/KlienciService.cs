﻿using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace KlienciService
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class KlienciService : IKlienciService
    {

        public KlienciService()
        {
        }

        public string CheckConnection()
        {
            return "Hello from KlienciService, " + DateTime.Now;
        }

        public int DodajKlienta(Klienci klient)
        {
            Klienci dodanyKlient;
            try
            {
                using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
                {
                    dodanyKlient = _db.Klienci.Add(klient);
                    _db.Save();
                }
            }
            catch (Exception)
            {
                return -1;
            }
            return dodanyKlient.KlientId;
        }

        public bool UsunKlienta(int id)
        {
            try
            {
                using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
                {
                    _db.Klienci.Remove(_db.Klienci.Where(x => x.KlientId == id).First());
                    _db.Save();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool UsunKlientow(Predicate<Klienci> filtr)
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                try
                {
                    Predicate<Klienci> a = filtr as Predicate<Klienci>;
                    var removedElem = _db.Klienci.ToList().FindAll(a);
                    var count = removedElem.Count;
                    removedElem.ForEach(x => _db.Klienci.Remove(x));
                    _db.SaveChanges();

                }
                catch (Exception)
                {
                    return true;
                }
                return false;
            }

        }

        public bool EdytujKlienta(int id, Klienci klient)
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                try
                {
                    var customer = _db.Klienci.First(x => x.KlientId == id);
                    var entity = _db.Entry<Klienci>(customer).Entity;
                    entity.Branza_Id = klient.Branza_Id;
                    entity.Nazwa_Klienta = klient.Nazwa_Klienta;
                    entity.NIP = klient.NIP;
                    entity.REGON = klient.REGON;
                    entity.Status_Klienta_Id = klient.Status_Klienta_Id;
                    entity.Strona_WWW = klient.Strona_WWW;
                    entity.Uwagi = klient.Uwagi;
                    _db.SaveChanges();
                }
                catch (Exception)
                {

                    return false;
                }
                return true;
            }
        }

        public List<Klienci> ZnajdzKlientow(int uzytkownikId)
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var klients = _db.Klienci.Include("StatusKlienta").Include("BranzaKlienta").Include("Uzytkownicy").
                   Where(x => x.UzytkownikId == uzytkownikId && x.Uzytkownicy != null).Select(x => x).ToList();
                var kl = new List<Klienci>();
                klients.ForEach(x =>
                {
                    kl.Add(new Klienci()
                    {

                        Branza_Id = x.Branza_Id,
                        BranzaKlienta = new BranzaKlienta()
                        {
                            Branza = x.BranzaKlienta.Branza,
                            BranzaID = x.BranzaKlienta.BranzaID,
                            Klienci = null
                        },
                        KlientId = x.KlientId,
                        Nazwa_Klienta = x.Nazwa_Klienta,
                        NIP = x.NIP,
                        REGON = x.REGON,
                        Status_Klienta_Id = x.Status_Klienta_Id,
                        StatusKlienta = new StatusKlienta()
                        {
                            Klienci = null,
                            Status = x.StatusKlienta.Status,
                            StatusId = x.StatusKlienta.StatusId
                        },
                        Strona_WWW = x.Strona_WWW,
                        Uwagi = x.Uwagi,
                        UzytkownikId = x.UzytkownikId,
                        Uzytkownicy = null
                    });
                });
                return kl;
            }
        }

        public List<Klienci> ZwrocWszystkichKlientow() 
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var klients = _db.Klienci.Include("StatusKlienta").Include("BranzaKlienta").Include("Uzytkownicy").Select(x => x).ToList();
                var kl = new List<Klienci>();
                klients.ForEach(x =>
                {
                    kl.Add(new Klienci()
                    {
                        Branza_Id = x.Branza_Id,
                        BranzaKlienta = new BranzaKlienta()
                        {
                            Branza = x.BranzaKlienta.Branza,
                            BranzaID = x.BranzaKlienta.BranzaID,
                            Klienci = null
                        },
                        KlientId = x.KlientId,
                        Nazwa_Klienta = x.Nazwa_Klienta,
                        NIP = x.NIP,
                        REGON = x.REGON,
                        Status_Klienta_Id = x.Status_Klienta_Id,
                        StatusKlienta = new StatusKlienta()
                        {
                            Klienci = null,
                            Status = x.StatusKlienta.Status,
                            StatusId = x.StatusKlienta.StatusId
                        },
                        Strona_WWW = x.Strona_WWW,
                        Uwagi = x.Uwagi,
                        UzytkownikId = x.UzytkownikId,
                        Uzytkownicy = null
                    });
                });
                return kl;
            }
        }

        // dodane przez TheLegenda
        public int DodajAdres(Adresy adres)
        {
            Adresy id;
            try
            {
                using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
                {
                    id = _db.Adresy.Add(adres);
                    _db.SaveChanges();
                }
            }
            catch (Exception)
            {
                return -1;
            }
            return id.AdresId;
        }

        public bool UsunAdres(int id)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                try
                {
                    var adres = context.Adresy.Where(x => x.AdresId == id).First();
                    context.Adresy.Remove(adres);
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }
        }

        public bool EdytujAdres(int id, Adresy adres)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                try
                {
                    var ad = context.Adresy.First(x => x.AdresId == id);
                    var adresDoedycji = context.Entry<Adresy>(ad).Entity;
                    adresDoedycji.Email = adres.Email;
                    adresDoedycji.Kod_Pocztowy = adres.Kod_Pocztowy;
                    adresDoedycji.Miejscowość = adres.Miejscowość;
                    adresDoedycji.PanstwoId = adres.PanstwoId;
                    adresDoedycji.RodzajAdresuID = adres.RodzajAdresuID;
                    adresDoedycji.Telefon = adres.Telefon;
                    adresDoedycji.Ulica_i_Numer = adres.Ulica_i_Numer;
                    adresDoedycji.WojewodztwoId = adres.WojewodztwoId;
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    return false;
                }
                return true;

            }

        }

        public List<Adresy> ZnajdzAdresy(int klientId)
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var adresyId = _db.AdresyKlientow.ToList().FindAll(x => x.KlientId == klientId).Select(p => p.AdresId).ToList();
                var adresy = _db.Adresy.Include("Panstwa").Include("Wojewodztwa").Include("RodzajAdresu")
                    .Where(x => adresyId.Contains(x.AdresId)).ToList();
                var adres = new List<Adresy>();
                adresy.ForEach(x => adres.Add(new Adresy()
                {
                    AdresId = x.AdresId,
                    Email = x.Email,
                    Kod_Pocztowy = x.Kod_Pocztowy,
                    Miejscowość = x.Miejscowość,
                    Panstwa = new Panstwa() { Adresy = null, Panstwo = x.Panstwa.Panstwo, PanstwoID = x.Panstwa.PanstwoID },
                    PanstwoId = x.PanstwoId,
                    RodzajAdresu = new RodzajAdresu() { Adresy = null, Rodzaj = x.RodzajAdresu.Rodzaj, RodzajAdresuID = x.RodzajAdresu.RodzajAdresuID },
                    RodzajAdresuID = x.RodzajAdresuID,
                    Telefon = x.Telefon,
                    Ulica_i_Numer = x.Ulica_i_Numer,
                    Wojewodztwa = new Wojewodztwa() { Adresy = null, Id = x.Wojewodztwa.Id, Wojewodztwo = x.Wojewodztwa.Wojewodztwo },
                    WojewodztwoId = x.WojewodztwoId
                }));
                return adres;
            }
        }

        public bool PrzypiszAdresy(int klientId, List<int> adresyId)
        {
            if (adresyId == null || klientId == 0 || adresyId.Count == 0)
            {
                return false;
            }
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                try
                {
                    foreach (int item in adresyId)
                    {
                        context.AdresyKlientow.Add(new AdresyKlientow() { AdresId = item, KlientId = klientId });
                    }
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    return false;
                }

                return true;
            }
        }

        #region Slowniki
        public List<BranzaKlienta> PobierzBranze()
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var branze = _db.BranzaKlienta.ToList();
                var newBranze = new List<BranzaKlienta>();
                branze.ForEach(x => newBranze.Add(new BranzaKlienta() { BranzaID = x.BranzaID, Klienci = x.Klienci.ToList(), Branza = x.Branza }));
                return newBranze;
            }

        }

        public List<Wojewodztwa> PobierzWojewodztwa()
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var wojewodztwa = _db.Wojewodztwa.ToList();
                var newWojewodztwa = new List<Wojewodztwa>();
                wojewodztwa.ForEach(x => newWojewodztwa.Add(new Wojewodztwa() { Id = x.Id, Adresy = x.Adresy.ToList(), Wojewodztwo = x.Wojewodztwo }));
                return newWojewodztwa;
            }
        }

        public List<Panstwa> PobierzPanstwa()
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var panstwa = _db.Panstwa.ToList();
                var newPanstwa = new List<Panstwa>();
                panstwa.ForEach(x => newPanstwa.Add(new Panstwa() { PanstwoID = x.PanstwoID, Adresy = x.Adresy.ToList(), Panstwo = x.Panstwo }));
                return newPanstwa;
            }
        }

        public List<StatusKlienta> PobierzStatusy()
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var statusy = _db.StatusKlienta.ToList();
                var newStatusy = new List<StatusKlienta>();
                statusy.ForEach(x => newStatusy.Add(new StatusKlienta() { StatusId = x.StatusId, Klienci = x.Klienci.ToList(), Status = x.Status }));
                return newStatusy;
            }
        }

        public List<RodzajAdresu> PobierzRodzaje()
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var rodzaje = _db.RodzajAdresu.ToList();
                var newRodzaje = new List<RodzajAdresu>();
                rodzaje.ForEach(x => newRodzaje.Add(new RodzajAdresu() { RodzajAdresuID = x.RodzajAdresuID, Adresy = x.Adresy.ToList(), Rodzaj = x.Rodzaj }));
                return newRodzaje;
            }
        }
        #endregion





        public bool UsunPrzypisaniePoIdKlienta(int klientId)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                try
                {
                    context.AdresyKlientow.Remove(context.AdresyKlientow.Where(x => x.KlientId == klientId).First());
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    return false;

                }
                return true;
            }
        }

        public bool UsunPrzypisaniePoIdAdresu(int adresId)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                try
                {
                    context.AdresyKlientow.Remove(context.AdresyKlientow.Where(x => x.AdresId == adresId).First());
                    context.SaveChanges();
                }
                catch (Exception)
                {
                    return false;

                }
                return true;
            }
        }
    }
}

