﻿using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace LogService
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class LogService : ILogService
    {
        public void Log(List<Historia> historia)
        {
            using (CustomerManagmentDataEntities2 contex = new CustomerManagmentDataEntities2())
            {
                foreach (var item in historia)
                {
                    contex.Historia.Add(item);
                }
                contex.SaveChanges();
            }
        }


        public List<Historia> GetLogs(int userId)
        {
            using (CustomerManagmentDataEntities2 context= new CustomerManagmentDataEntities2())
            {
                return context.Historia.Where(x => x.UzytkownikID == userId).ToList();

            }
        }
    }
}
