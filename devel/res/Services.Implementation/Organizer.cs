﻿using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace OrganizerService
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class Organizer : IOrganizer
    {


        public Organizer()
        { }

        public List<Spotkanies> PobierzSpotkania(int userId)
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var db = _db.Uzytkownicy.Include("Spotkanies").Include("Notatki");
                var spotk = db.Where(x => x.UzytkownikId == userId).First().Spotkanies.ToList();

                var spotkania = new List<Spotkanies>();

                spotk.ToList().ForEach(x =>
                {
                    spotkania.Add(
                        new Spotkanies()
                        {
                            Data = x.Data,
                            DlugoscTrwania = x.DlugoscTrwania,
                            Kolor = x.Kolor,
                            Nazwa = x.Nazwa,
                            SpotkanieID = x.SpotkanieID,
                            UzytkownikId = x.UzytkownikId,
                            Uzytkownicy = null
                        });
                });
                return spotkania;
            }
        }

        public List<Notatki> PobierzNotatki(int userId)
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var notatki = new List<Notatki>();
                _db.Uzytkownicy.Include("Notatki").Where(x => x.UzytkownikId == userId).
                                FirstOrDefault().Notatki.ToList().ForEach(x => notatki.Add(
                                    new Notatki()
                                    {
                                        UzytkownikId = x.UzytkownikId,
                                        Tytul = x.Tytul,
                                        Notatka = x.Notatka,
                                        Data_dodania = x.Data_dodania,
                                        NotatkaID = x.NotatkaID,
                                        Uzytkownicy = null
                                    }));
                return notatki;
            }
        }

        public int DodajSpotkanie(Spotkanies spotkanie)
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                _db.Spotkanies.Add(spotkanie);
                _db.Entry<Spotkanies>(spotkanie).State = System.Data.EntityState.Added;
                _db.SaveChanges();
                return spotkanie.SpotkanieID;
            }
        }

        public int DodajNotatke(Notatki notatka)
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                _db.Notatki.Add(notatka);
                _db.SaveChanges();
                return notatka.NotatkaID;
            }
        }


        public bool EdytujSpotkanie(int id, Spotkanies spotkanie)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                var spotkanieDB = context.Spotkanies.Where(x => x.SpotkanieID == id).FirstOrDefault();
                if (spotkanieDB == null)
                {
                    return false;
                }

                try
                {
                    var entity = context.Entry<Spotkanies>(spotkanieDB).Entity;
                    entity.Nazwa = spotkanie.Nazwa;
                    entity.Kolor = spotkanie.Kolor;

                    context.SaveChanges();
                    return true;
                }
                catch (Exception)
                {

                    return false;
                }
            }
        }


        public int DodajZadanie(Zadania zadanie)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                context.Zadania.Add(zadanie);
                context.Entry<Zadania>(zadanie).State = System.Data.EntityState.Added;

                context.SaveChanges();
                return zadanie.ZadanieId;
            }
        }

        public bool UsunZadanie(string tekst, int uzytkownikId)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                try
                {
                    var entity = context.Zadania.First(x => x.Tekst == tekst && x.UzytkownikId == uzytkownikId);
                    context.Zadania.Remove(entity);
                    context.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }


        public List<Zadania> PobierzZadania(int userID)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                List<Zadania> znalezioneZadania = new List<Zadania>();
                var zadania = context.Uzytkownicy.Include("Zadania").First(x => x.UzytkownikId == userID).Zadania.ToList<Zadania>();
                zadania.ForEach(x => znalezioneZadania.Add(new Zadania()
                {
                    Data = x.Data,
                    Priorytet = x.Priorytet,
                    Tekst = x.Tekst,
                    UzytkownikId = x.UzytkownikId,
                    Uzytkownicy = null,
                    ZadanieId = x.ZadanieId
                }));

                return znalezioneZadania;
            }
        }
    }
}