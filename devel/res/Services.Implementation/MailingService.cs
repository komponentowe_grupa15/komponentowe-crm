﻿using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MailingService
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
   public class MailingService : IMailingService
    {
       
        public void WyslijWiadomosc(Wiadomosci wiadomosc)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                context.Wiadomosci.Add(wiadomosc);
                context.Entry<Wiadomosci>(wiadomosc).State = System.Data.EntityState.Added;
                context.SaveChanges();
            }
        }

        public void WyslijWiadomosci(List<CustomerRelationshipManagement.DAL.Wiadomosci> wiadomosc)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                foreach (var item in wiadomosc)
                {
                    context.Wiadomosci.Add(item);
                }
                context.SaveChanges();
            }
        }

        public List<CustomerRelationshipManagement.DAL.Wiadomosci> PobierzWiadomosci(string userName)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                var wiadomosci =  context.Wiadomosci.Where(x=>x.DoKogo.Trim().ToLowerInvariant() == userName.Trim().ToLowerInvariant()).ToList();
                var wiad = new List<Wiadomosci>();
                wiadomosci.ForEach(x => wiad.Add(new Wiadomosci() { Data = x.Data, DoKogo = x.DoKogo, OdKogo = x.OdKogo, Temat = x.Temat, Tresc = x.Tresc, WadiomoscID = x.WadiomoscID }));
                return wiad;
            }
            
        }

        public List<CustomerRelationshipManagement.DAL.Wiadomosci> PobierzWiadomosciPrzezFiltr(Predicate<CustomerRelationshipManagement.DAL.Wiadomosci> filtr)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                return context.Wiadomosci.Where(filtr.Invoke).ToList();
            }
        }
    }
}
