﻿using CustomerRelationshipManagement.DAL;
using CustomService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CustomService
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class CustomService : ICustomService
    {

        public CustomService()
        {
        }

        public bool ValidateLogin(string userName, string password)
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var user = _db.Uzytkownicy.Where(x => x.Login == userName).FirstOrDefault();
                if (user == null)
                {
                    return false;
                }
                var haslo = _db.Hasla.First(x => x.UzytkownikId == user.UzytkownikId);
                if (haslo == null)
                {
                    return false;
                }
                string passwordString = userName + " : " + password + " : " + haslo.Salt;
                string hash = CreateMD5Hash(passwordString);


                return 0 == string.Compare(hash, haslo.Hash, true, CultureInfo.InvariantCulture);
            }
        }

        public string CheckConnection()
        {
            return "hello from CustomService, " + DateTime.Now;
        }

        public Uzytkownicy PobierzUzytkownika(string login)
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var db = _db.Uzytkownicy.Include("Spotkanies").Include("Notatki");

                var user = db.Where(x => x.Login == login).FirstOrDefault();
             
                var notatki = new List<Notatki>();
                notatki.AddRange(user.Notatki.Where(x => x.NotatkaID != 0));
                var spotkania = new List<Spotkanies>();
                spotkania.AddRange(user.Spotkanies.ToList());
                return new Uzytkownicy()
                {
                    UzytkownikId = user.UzytkownikId,
                    OsobaId = user.OsobaId,
                    Login = user.Login,
                    Aktywny = user.Aktywny,
                    
                };
            }
        }



        public string CreateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public List<string> PobierzUzytkownikow()
        {
            using (CustomerManagmentDataEntities2 context =  new CustomerManagmentDataEntities2())
            {
                return context.Uzytkownicy.Select(x => x.Osoba.Imie + " " + x.Osoba.Nazwisko).ToList<string>();
            }
        }


        public string PobierzImieINazwisko(int userId)
        {
            using (CustomerManagmentDataEntities2 context = new CustomerManagmentDataEntities2())
            {
                return context.Uzytkownicy.Where( o=>o.UzytkownikId == userId).Select(x => x.Osoba.Imie + " " + x.Osoba.Nazwisko).First();
            }
        }
    }


}

