﻿using ImportEksportService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using System.IO;
using System.Data;
using CustomerRelationshipManagement.DAL;
using System.Reflection;
using System.ServiceModel;

namespace ImportEksportService
{
    public class ImportEksportService : IImportEksport
    {
        public string CheckConnection()
        {
            return "Hello from ImportEksportService, " + DateTime.Now;
        }

        public bool Importuj(string file)
        {
            throw new NotImplementedException();
        }

        public byte[] Eksportuj(int uzytkownikId, string extension)
        {
            using (CustomerManagmentDataEntities2 _db = new CustomerManagmentDataEntities2())
            {
                var klients = _db.Klienci.Include("StatusKlienta").Include("BranzaKlienta").Include("Uzytkownicy").
                   Where(x => x.UzytkownikId == uzytkownikId && x.Uzytkownicy != null).Select(x => x).ToList();
                //var klienci = new List<Klienci>();
                //klients.ForEach(x =>
                //{
                //    klienci.Add(new Klienci()
                //    {

                //        Branza_Id = x.Branza_Id,
                //        BranzaKlienta =null,//_db.BranzaKlienta.Where(c=>c.BranzaID == x.Branza_Id).First(),
                //        //{
                //        //    Branza = x.BranzaKlienta.Branza,
                //        //    BranzaID = x.BranzaKlienta.BranzaID,
                //        //    Klienci = null
                //        //},
                //        KlientId = x.KlientId,
                //        Nazwa_Klienta = x.Nazwa_Klienta,
                //        NIP = x.NIP,
                //        REGON = x.REGON,
                //        Status_Klienta_Id = x.Status_Klienta_Id,
                //         StatusKlienta = null, 
                //            //new StatusKlienta()
                //        //{
                //        //    Klienci = null,
                //        //    Status = x.StatusKlienta.Status,
                //        //    StatusId = x.StatusKlienta.StatusId
                //        //},
                //        Strona_WWW = x.Strona_WWW,
                //        Uwagi = x.Uwagi,
                //        UzytkownikId = x.UzytkownikId,
                //        Uzytkownicy = null//_db.Uzytkownicy.Where(u=>u.UzytkownikId == x.UzytkownikId).First()
                //    });
                //});
                //return klienci;
                byte[] stream = null;
                switch (extension)
                {
                    case ".xlsx":
                        {
                            stream = SaveToExcelFormat(klients);
                        } break;
                    case ".csv":
                        {
                            stream = SaveToCsvFormat(klients);
                        } break;
                    default:
                        break;
                }
                
                return stream;
            }
            
        
        }

        private byte[] SaveToExcelFormat(List<Klienci> klienci)
        {
            var nowiKlienci = klienci.Select(x => new
            {
                x.KlientId,
                x.Nazwa_Klienta,
                x.BranzaKlienta.Branza,
                x.NIP,
                x.REGON,
                x.Strona_WWW,
                x.StatusKlienta.Status,
                x.Uwagi
            }).ToList();
            //Stream stream = new MemoryStream();
            ExcelPackage epc = new ExcelPackage();
            var ws = epc.Workbook.Worksheets.Add("Data");
            ws.Cells["A1"].LoadFromCollection(nowiKlienci, true);
            //epc.SaveAs(stream);
            //var vv = epc.GetAsByteArray();
            //Stream d = new MemoryStream(epc.GetAsByteArray());
            //epc.Save();
            return epc.GetAsByteArray();
            //StreamReader sr = new StreamReader(new MemoryStream(epc.GetAsByteArray()));
            //return sr.ReadToEnd();
        }

        private byte[] SaveToCsvFormat(List<Klienci> klienci, string separator = ";")
        {
            var nowiKlienci = klienci.Select(x => new 
                   {
                       x.KlientId,
                       x.Nazwa_Klienta,
                       x.BranzaKlienta.Branza,
                       x.NIP, 
                       x.REGON,
                       x.Strona_WWW,
                       x.StatusKlienta.Status,
                       x.Uwagi
                   }).ToList();

            StringBuilder sb = new StringBuilder();
            PropertyInfo[] properties = null;

            if (nowiKlienci != null || nowiKlienci.Count != 0)
            {
                properties = nowiKlienci[0].GetType().GetProperties();
                foreach (var klient in nowiKlienci)
                {
                    List<string> allPropsStrings = new List<string> { };
                    foreach (var name in properties)
                    {
                        PropertyInfo props = klient.GetType().GetProperty(name.Name);
                        if (props.GetValue(klient) != null)
                        {
                            allPropsStrings.Add(props.GetValue(klient).ToString());
                        }
                        else allPropsStrings.Add(string.Empty);
                        
                    }
                    sb.AppendLine(string.Join(separator, allPropsStrings.ToArray()));
                }
            }
            //Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString()));

            return Encoding.UTF8.GetBytes(sb.ToString());//sb.ToString();
        }
    }
}
