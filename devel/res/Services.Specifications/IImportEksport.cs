﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.IO;

namespace ImportEksportService
{
    [ServiceContract]
    public interface IImportEksport
    {
        [OperationContract]
        string CheckConnection();

        [OperationContract]
        bool Importuj(string file);

        [OperationContract]
        byte[] Eksportuj(int uzytkownikId, string extension);
    }
}