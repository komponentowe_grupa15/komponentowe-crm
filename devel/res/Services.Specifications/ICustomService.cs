﻿using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CustomService
{
    [ServiceContract]
    public interface ICustomService
    {
        [OperationContract]
        string CheckConnection();
        [OperationContract]
        bool ValidateLogin(string userName, string password);
        [OperationContract]
        Uzytkownicy PobierzUzytkownika(string login);
        [OperationContract]
        string PobierzImieINazwisko(int userId);
        [OperationContract]
        List<string> PobierzUzytkownikow();
    }
   
}
