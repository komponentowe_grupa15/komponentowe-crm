﻿using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MailingService
{
    [ServiceContract]
    public interface IMailingService
    {
        [OperationContract]
        void WyslijWiadomosc(Wiadomosci wiadomosc);

        [OperationContract]
        void WyslijWiadomosci(List<Wiadomosci> wiadomosc);

        [OperationContract]
        List<Wiadomosci> PobierzWiadomosci(string userName);

        [OperationContract]
        List<Wiadomosci> PobierzWiadomosciPrzezFiltr(Predicate<Wiadomosci> filtr);
    }
}
