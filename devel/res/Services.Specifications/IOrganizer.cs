﻿
using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace OrganizerService
{
    [ServiceContract]
    public interface IOrganizer
    {

        [OperationContract]
        List<Spotkanies> PobierzSpotkania(int userId);

        [OperationContract]
        bool EdytujSpotkanie(int id, Spotkanies spotkanie);

        [OperationContract]
        int DodajSpotkanie(Spotkanies spotkanie);

        [OperationContract]
        List<Notatki> PobierzNotatki(int userId);

        [OperationContract]
        int DodajNotatke(Notatki notatka);

        [OperationContract]
        int DodajZadanie(Zadania zadanie);

        [OperationContract]
        bool UsunZadanie(string tekst, int uzytkownikId);

        [OperationContract]
        List<Zadania> PobierzZadania(int userID);

    }




}
