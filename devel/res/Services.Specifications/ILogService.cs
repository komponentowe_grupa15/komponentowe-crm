﻿using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace LogService
{
    [ServiceContract]
    public interface ILogService
    {
        [OperationContract]
        void Log(List<Historia> historia);
        
        [OperationContract]
        List<Historia> GetLogs(int userId);
    }
}
