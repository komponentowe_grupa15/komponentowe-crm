﻿using CustomerRelationshipManagement.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace KlienciService
{
    [ServiceContract]
    public interface IKlienciService
    {
        [OperationContract]
        string CheckConnection();

        #region Klienci
        [OperationContract]
        int DodajKlienta(Klienci klient);

        [OperationContract]
        bool UsunKlienta(int id);

        [OperationContract]
        bool UsunKlientow(Predicate<Klienci> filtr);

        [OperationContract]
        bool EdytujKlienta(int id, Klienci klient);

        [OperationContract]
        List<Klienci> ZnajdzKlientow(int uzytkownikId);

        [OperationContract]
        List<Klienci> ZwrocWszystkichKlientow();
        #endregion

        #region Adresy
        [OperationContract]
        int DodajAdres(Adresy adres);

        [OperationContract]
        bool UsunAdres(int id);

        [OperationContract]
        bool EdytujAdres(int id, Adresy adres);

        [OperationContract]
        List<Adresy> ZnajdzAdresy(int klientId);

        [OperationContract]
        bool PrzypiszAdresy(int klientId, List<int> adresyId);

        [OperationContract]
        bool UsunPrzypisaniePoIdKlienta(int klientId);

        [OperationContract]
        bool UsunPrzypisaniePoIdAdresu(int adresId);
        #endregion

        #region Słowniki
        [OperationContract]
        List<BranzaKlienta> PobierzBranze();

        [OperationContract]
        List<Wojewodztwa> PobierzWojewodztwa();

        [OperationContract]
        List<Panstwa> PobierzPanstwa();

        [OperationContract]
        List<StatusKlienta> PobierzStatusy();

        [OperationContract]
        List<RodzajAdresu> PobierzRodzaje();
        #endregion
    }
}
